/** File generate by <hotlala8088@gmail.com> 2015/01/13  
 */

#ifndef __WORLDCONN_H__
#define __WORLDCONN_H__


#include "ARPC_Pre.h"
#include "CON_ARPCConnection.h"
#include "proto.h"

//-------------------------------------------------------------------------
/** 注意 先有连接然后才会有 player 
 *  连接建立后会有登陆验证,验证通过后才会建立player进入game logic
 */

class Player;
class ClientHandler 
	: public CON_ARPCChannel< Server2ClientStub, Client2ServerProxy >
	, public Client2ServerProxy
{
public:
	enum State
	{
		Idle,
		Login,
		Game,
		Logout,
		Illegal,
	};
public:
	ClientHandler(){setProxy(this);}
public:
public:
	#include "Client2ServerMethods.h"
public:
	Player	*player_;
};

#endif // endif __WORLDCONN_H__
