/** File generate by <hotlala8088@gmail.com> 2015/01/13  
 */
#include "config.h"
#include "worldserv.h"
#include "client.h"
#include "gwhandler.h"
//-------------------------------------------------------------------------
/**
 */
int WorldServ::init (int argc, ACE_TCHAR *argv[])
{
	ACE_DEBUG((LM_TRACE,ACE_TEXT("Init world serv... ")));

	acceptgw();
	
	/// \初始化记时器
	ACE_Time_Value t;
	t.set((double)1.0/(double)(TIMER_FREQ));
	this->reactor()->schedule_timer(this, NULL, t, t);
	
	
	ACE_DEBUG((LM_TRACE,ACE_TEXT("Init world serv succ... ")));
	return 0;
}

//-------------------------------------------------------------------------
/**
 */
int WorldServ::fini (void)
{
	ACE_DEBUG((LM_TRACE,ACE_TEXT("Fini world serv... ")));

	// 退出计时器.
	this->reactor()->cancel_timer(this);

	return 0;
}


//-------------------------------------------------------------------------
/**
 */
int WorldServ::handle_timeout (const ACE_Time_Value &current_time, const void *act)
{

	return 0;
}

//-------------------------------------------------------------------------
/**
 */
class GatewayAcceptor
	: public ACE_Acceptor< GatewayHandler , ACE_SOCK_ACCEPTOR >
{
public:
	int make_svc_handler(GatewayHandler *&sh)
	{
		//sh = WorldServ::instance()->gwhandler();
		if( ACE_Acceptor<GatewayHandler, ACE_SOCK_ACCEPTOR>::make_svc_handler(sh) == -1)
		{
			return -1;
		}
		return 0;
	}
};
GatewayAcceptor acceptor;

void WorldServ::acceptgw()
{
	
	ACE_INET_Addr addr(LISTEN_GATEWAY);
	if(acceptor.open(addr) == -1)
	{
		ACE_DEBUG((LM_INFO,ACE_TEXT("World serv listen %s field\n"),LISTEN_GATEWAY));
		return;
	}

}