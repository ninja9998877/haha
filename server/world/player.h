/** File generate by <hotlala8088@gmail.com> 2015/01/13  
 */

#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "config.h"
#include "client.h"


class Player
{
public:
	typedef std::map< std::string, Player* > Map;
	typedef std::pair< std::string, Player*> Pair;
	
	static Map playerStore_;
	
public:

public:
	Player(ClientHandler *);
	
	ClientHandler *handler_;
	std::string name_;
};

#endif