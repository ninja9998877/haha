/** File generate by <hotlala8088@gmail.com> 2015/01/13  
 */

#include "config.h"
#include "worldserv.h"
#include "ace/Service_Config.h"
#include "ace/Select_Reactor.h"
#include "ace/Dev_Poll_Reactor.h"

class SignalHandler : public ACE_Event_Handler
{
public:
	virtual int handle_signal (int sig, siginfo_t *, ucontext_t *)
	{
		// 结束事件循环.
		ACE_Reactor::end_event_loop();
		return 0;
	}
};

int ACE_TMAIN (int argc, ACE_TCHAR *argv[])
{
#ifdef ACE_WIN32
	ACE_Reactor::instance(new ACE_Reactor(new ACE_Select_Reactor(), 1), 1);
#elif defined (ACE_HAS_EVENT_POLL) || defined (ACE_HAS_DEV_POLL)
	ACE_Reactor::instance(new ACE_Reactor(new ACE_Dev_Poll_Reactor, 1), 1);
#else
	ACE_Reactor::instance();
#endif

	SignalHandler sh;
	ACE_Reactor::instance()->register_handler(SIGINT, &sh);
	
	WorldServ::instance()->reactor(ACE_Reactor::instance());
	if(WorldServ::instance()->init(argc,argv) == -1)
	{
		ACE_DEBUG((LM_INFO,ACE_TEXT("Init WorldServ err.")));
		return -1;
	}

	// 运行ractor event loop.
	ACE_Reactor::run_event_loop();
	return 0;
}