/** File generate by <hotlala8088@gmail.com> 2015/01/17  
 */

#ifndef __GATEWAY_HANDLER_H__
#define __GATEWAY_HANDLER_H__

#include "config.h"
#include "client.h"

class GatewayHandler
	: public CON_ChannelConnection
{
public:
	static GatewayHandler* instance(){static GatewayHandler handler; return &handler;}
public:
	GatewayHandler():CON_ChannelConnection(false){}
public:
	CON_Channel* makeChannel();
	void acceptChannel(CON_Channel* c, void* data, size_t size);
	int handleReceived(void* data, size_t size);
};



#endif