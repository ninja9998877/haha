/** File generate by <hotlala8088@gmail.com> 2015/01/13  
 */

#ifndef __WORLDSERV_H__
#define __WORLDSERV_H__

#include "config.h"
#include "client.h"
#include "gwhandler.h"
class WorldServ 
	: public ACE_Service_Object
{
public:
	static WorldServ* instance() {static WorldServ serv; return &serv;}

public:
	int init (int argc, ACE_TCHAR *argv[]);
	int fini (void);
	int handle_timeout (const ACE_Time_Value &current_time, const void *act);

public:
	void acceptgw();
	GatewayHandler *gwhandler(){return &gwhandler_;}
public:

	GatewayHandler						 gwhandler_;
	
};

#endif /// endif __WORLDSERV_H__