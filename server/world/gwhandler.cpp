
#include "config.h"
#include "gwhandler.h"

CON_Channel*
GatewayHandler::makeChannel()
{
	return new ClientHandler();
}

void
GatewayHandler::acceptChannel(CON_Channel* c, void* data, size_t size)
{
	/// do nothing
}

int
GatewayHandler::handleReceived(void* data, size_t size)
{
	return CON_ChannelConnection::handleReceived(data,size);
}

