/** File generate by <hotlala8088@gmail.com> 2015/01/13  
 */


#ifndef __CASE_H__
#define __CASE_H__

#include "ARPC_Pre.h"
#include "CON_ARPCConnection.h"
#include "proto.h"

class CaseHandler 
	: public CON_ARPCConnection< Client2ServerStub, Server2ClientProxy >
	, public Server2ClientProxy
{
public:
	CaseHandler(){setProxy(this);}
public:
#include "Server2ClientMethods.h"
public:
};

#include "case.inl"

#endif