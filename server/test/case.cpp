/** File generate by <hotlala8088@gmail.com> 2015/01/13  
 */

//-------------------------------------------------------------------------
/** ����
 */

#include "case.h"
#include "ace/Service_Config.h"
#include "ace/Select_Reactor.h"
#include "ace/Dev_Poll_Reactor.h"
#include "ace/OS_NS_sys_stat.h"
#include "ace/SOCK_Connector.h"
#include "ace/Connector.h"
#include "config.h"
#define CONNECT_ADDR "127.0.0.1:9090"

int ACE_TMAIN (int argc, ACE_TCHAR *argv[])
{
#ifdef ACE_WIN32
	ACE_Reactor::instance(new ACE_Reactor(new ACE_Select_Reactor(), 1), 1);
#elif defined (ACE_HAS_EVENT_POLL) || defined (ACE_HAS_DEV_POLL)
	ACE_Reactor::instance(new ACE_Reactor(new ACE_Dev_Poll_Reactor, 1), 1);
#else
	ACE_Reactor::instance();
#endif
	CaseHandler *handler = NULL;
	ACE_Connector<CaseHandler, ACE_SOCK_CONNECTOR> caseConnector;
	ACE_INET_Addr addr(CONNECT_ADDR);
	if(-1 == caseConnector.connect(handler,addr))
	{
		SRV_ASSERT(0);
	}

	handler->login("123","123");
	handler->pong();
	ACE_Reactor::run_event_loop();
	
	return 0;
}