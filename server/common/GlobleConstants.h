/** File generate by <hotlala8088@gmail.com> 2015/01/14  
 */

#ifndef __GLOBLE_CONSTANTS_H__
#define __GLOBLE_CONSTANTS_H__

//-------------------------------------------------------------------------
/** 这里定义全局宏变量 
 *  例如 公式宏 等
 */

#define CALC_PARAMS(x) ( x * (x + x) ) 

#define LISTEN_CLIENT "0.0.0.0:9070"
#define LISTEN_GATEWAY "127.0.0.1:9080"
#define LISTEN_CLIENT_ADDR "127.0.0.1:9090"
#define TIMER_FREQ   20 



#endif