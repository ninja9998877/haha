/** File generate by <hotlala8088@gmail.com> 2015/01/13  
 */

#ifndef __CONFIG_H__
#define __CONFIG_H__

#if defined (_MSC_VER)
	// Skip window min max definition.
	#define NOMINMAX
	// Disable vc warnings.
	#define _CRT_SECURE_NO_WARNINGS
	#pragma warning(disable:4996)
	#pragma warning(disable:4267)
	#pragma warning(disable:4129)//unrecognized character escape sequence
	#pragma warning(disable:4819)//The file contains a character that cannot be represented in the current code page (936). Save the file in Unicode format to prevent data loss
	#include "coredump.h"
#endif

#define SRV_ASSERT(X)\
	do {			\
	__assert__(X,__FILE__,__LINE__);\
	} while (0)

inline void __assert__(bool _X_,const char* file, int line)
{
	if (!(_X_))
	{
#ifdef WIN32
		__asm{ int 3}
#else
		//raise(SIGABRT);
#endif
	}
}

#include "GlobleConstants.h"

#include <map>
#include <vector>

#include "ace/Service_Config.h"
#include "ace/Select_Reactor.h"
#include "ace/Dev_Poll_Reactor.h"
#include "ace/Acceptor.h"
#include "ace/SOCK_Acceptor.h"
#include "ace/Time_Value.h"
#include "ace/Log_Msg.h"
#include "ace/Event_Handler.h"
#include "ace/OS_NS_sys_stat.h"
#include "ace/SOCK_Connector.h"
#include "ace/Connector.h"


#include "ARPC_Pre.h"
#include "CON_Channel.h"
#include "CON_ARPCChannel.h"
#include "CON_ARPCConnection.h"
#include "CON_ChannelConnection.h"
#include "CON_ConnectionManager.h"



#endif ///__CONFIG_H__