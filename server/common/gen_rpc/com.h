/* This file is generated by arpcc, do not change manually! */
#ifndef __com_h__
#define __com_h__
#include "ARPC_ProtocolWriter.h"
#include "ARPC_ProtocolReader.h"
#include "ARPC_EnumInfo.h"
//=============================================================
// struct Gate2WorldInit
struct Gate2WorldInit
{
	// member list.
	// field ids.
	enum
	{
		FIDMAX = 0,
	};
	// serialization.
	void serialize(ARPC_ProtocolWriter* s) const;
	// deserialization.
	bool deserialize(ARPC_ProtocolReader* r);
	// serialization field.
	bool serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* s) const;
	// deserialization field.
	bool deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* r);
};
//=============================================================
// enum PlayerStatus
enum PlayerStatus
{
	PS_Idle,
	PS_Login,
	PS_Game,
	PS_Logout,
	PS_Illegal,
};
extern ARPC_EnumInfo enumPlayerStatus;
//=============================================================
// enum OccupationType
enum OccupationType
{
	OT_None,
	OT_HeavyArmor,
	OT_LightArmor,
	OT_Spell,
	OT_Max,
};
extern ARPC_EnumInfo enumOccupationType;
//=============================================================
// enum PropertyType
enum PropertyType
{
	PT_None,
	PT_NoSleep,
	PT_NoPetrifaction,
	PT_NoDrunk,
	PT_NoChaos,
	PT_NoForget,
	PT_Anima,
	PT_Endurance,
	PT_Intelligence,
	PT_Physical,
	PT_Power,
	PT_Strength,
	PT_Speed,
	PT_Magic,
	PT_Hp,
	PT_Mp,
	PT_Attack,
	PT_Defense,
	PT_Agile,
	PT_Spirit,
	PT_Reply,
	PT_Max,
};
extern ARPC_EnumInfo enumPropertyType;
//=============================================================
// enum EquipmentSlot
enum EquipmentSlot
{
	ES_None,
	ES_Head,
	ES_Body,
	ES_RighHand,
	ES_LeftHand,
	ES_Ornament_0,
	ES_Ornament_2,
	ES_Max,
};
extern ARPC_EnumInfo enumEquipmentSlot;
#endif
