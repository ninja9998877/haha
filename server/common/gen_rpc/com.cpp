/* arpcc auto generated cpp file. */
#include "ARPC_FieldMask.h"
#include "com.h"
//=============================================================
void Gate2WorldInit::serialize(ARPC_ProtocolWriter* __s__) const
{
}
bool Gate2WorldInit::deserialize(ARPC_ProtocolReader* __r__)
{
		return true;
}
bool Gate2WorldInit::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
	}
	return false;
}
bool Gate2WorldInit::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
	}
	return false;
}
//=============================================================
static void initFuncPlayerStatus(ARPC_EnumInfo* e)
{
		e->items_.push_back("PS_Idle");
		e->items_.push_back("PS_Login");
		e->items_.push_back("PS_Game");
		e->items_.push_back("PS_Logout");
		e->items_.push_back("PS_Illegal");
}
ARPC_EnumInfo enumPlayerStatus("PlayerStatus", initFuncPlayerStatus);
//=============================================================
static void initFuncOccupationType(ARPC_EnumInfo* e)
{
		e->items_.push_back("OT_None");
		e->items_.push_back("OT_HeavyArmor");
		e->items_.push_back("OT_LightArmor");
		e->items_.push_back("OT_Spell");
		e->items_.push_back("OT_Max");
}
ARPC_EnumInfo enumOccupationType("OccupationType", initFuncOccupationType);
//=============================================================
static void initFuncPropertyType(ARPC_EnumInfo* e)
{
		e->items_.push_back("PT_None");
		e->items_.push_back("PT_NoSleep");
		e->items_.push_back("PT_NoPetrifaction");
		e->items_.push_back("PT_NoDrunk");
		e->items_.push_back("PT_NoChaos");
		e->items_.push_back("PT_NoForget");
		e->items_.push_back("PT_Anima");
		e->items_.push_back("PT_Endurance");
		e->items_.push_back("PT_Intelligence");
		e->items_.push_back("PT_Physical");
		e->items_.push_back("PT_Power");
		e->items_.push_back("PT_Strength");
		e->items_.push_back("PT_Speed");
		e->items_.push_back("PT_Magic");
		e->items_.push_back("PT_Hp");
		e->items_.push_back("PT_Mp");
		e->items_.push_back("PT_Attack");
		e->items_.push_back("PT_Defense");
		e->items_.push_back("PT_Agile");
		e->items_.push_back("PT_Spirit");
		e->items_.push_back("PT_Reply");
		e->items_.push_back("PT_Max");
}
ARPC_EnumInfo enumPropertyType("PropertyType", initFuncPropertyType);
//=============================================================
static void initFuncEquipmentSlot(ARPC_EnumInfo* e)
{
		e->items_.push_back("ES_None");
		e->items_.push_back("ES_Head");
		e->items_.push_back("ES_Body");
		e->items_.push_back("ES_RighHand");
		e->items_.push_back("ES_LeftHand");
		e->items_.push_back("ES_Ornament_0");
		e->items_.push_back("ES_Ornament_2");
		e->items_.push_back("ES_Max");
}
ARPC_EnumInfo enumEquipmentSlot("EquipmentSlot", initFuncEquipmentSlot);
