/* arpcc auto generated cpp file. */
#include "ARPC_FieldMask.h"
#include "proto.h"
//=============================================================
void Client2ServerStub::pong()
{
	ARPC_ProtocolWriter* w = methodBegin();
	if(!w) return;
	ARPC_UINT8 pid = 0;
	w->writeType(pid);
	methodEnd();
}
void Client2ServerStub::login(const ARPC_STRING& username,const ARPC_STRING& userpwd)
{
	ARPC_ProtocolWriter* w = methodBegin();
	if(!w) return;
	ARPC_UINT8 pid = 1;
	w->writeType(pid);
	// serialize username
	{
		w->writeType(username);
	}
	// serialize userpwd
	{
		w->writeType(userpwd);
	}
	methodEnd();
}
bool Client2ServerProxy::pong(ARPC_ProtocolReader* __r__)
{
	return pong();
}
bool Client2ServerProxy::login(ARPC_ProtocolReader* __r__)
{
	ARPC_STRING username;
	ARPC_STRING userpwd;
	// deserialize username
	{
		if(!__r__->readType(username, 65535)) return false;
	}
	// deserialize userpwd
	{
		if(!__r__->readType(userpwd, 65535)) return false;
	}
	return login(username,userpwd);
}
bool Client2ServerProxy::dispatch(ARPC_ProtocolReader* r)
{
	ARPC_UINT8 pid;
	if(!r->readType(pid)) return false;
	switch(pid)
	{
		case 0:
		{
			if(!pong(r)) return false;
		}
		break;
		case 1:
		{
			if(!login(r)) return false;
		}
		break;
		default: return false;
	}
	return true;
}
//=============================================================
void Server2ClientStub::ping()
{
	ARPC_ProtocolWriter* w = methodBegin();
	if(!w) return;
	ARPC_UINT8 pid = 0;
	w->writeType(pid);
	methodEnd();
}
void Server2ClientStub::loginok()
{
	ARPC_ProtocolWriter* w = methodBegin();
	if(!w) return;
	ARPC_UINT8 pid = 1;
	w->writeType(pid);
	methodEnd();
}
bool Server2ClientProxy::ping(ARPC_ProtocolReader* __r__)
{
	return ping();
}
bool Server2ClientProxy::loginok(ARPC_ProtocolReader* __r__)
{
	return loginok();
}
bool Server2ClientProxy::dispatch(ARPC_ProtocolReader* r)
{
	ARPC_UINT8 pid;
	if(!r->readType(pid)) return false;
	switch(pid)
	{
		case 0:
		{
			if(!ping(r)) return false;
		}
		break;
		case 1:
		{
			if(!loginok(r)) return false;
		}
		break;
		default: return false;
	}
	return true;
}
