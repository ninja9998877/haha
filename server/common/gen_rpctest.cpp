/* arpcc auto generated cpp file. */
#include "ARPC_FieldMask.h"
#include "test.h"
//=============================================================
COM_Field::COM_Field():
key_(0)
{}
void COM_Field::serialize(ARPC_ProtocolWriter* __s__) const
{
	// serialize key_
	{
		__s__->writeType(key_);
	}
	// serialize value_
	{
		ARPC_UINT32 __len__ = (ARPC_UINT32)value_.size();
		__s__->writeDynSize(__len__); 
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			__s__->writeType(value_[i]);
		}
	}
}
bool COM_Field::deserialize(ARPC_ProtocolReader* __r__)
{
	// deserialize key_
	{
		if(!__r__->readType(key_)) return false;
	}
	// deserialize value_
	{
		ARPC_UINT32 __len__;
		if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
		value_.resize(__len__);
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			if(!__r__->readType(value_[i])) return false;
		}
	}
		return true;
}
bool COM_Field::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_key_:
		{
			// serialize key_
			{
				__s__->writeType(key_);
			}
		}
		return true;
		case FID_value_:
		{
			// serialize value_
			{
				ARPC_UINT32 __len__ = (ARPC_UINT32)value_.size();
				__s__->writeDynSize(__len__); 
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					__s__->writeType(value_[i]);
				}
			}
		}
		return true;
	}
	return false;
}
bool COM_Field::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_key_:
		{
			// deserialize key_
			{
				if(!__r__->readType(key_)) return false;
			}
		}
		return true;
		case FID_value_:
		{
			// deserialize value_
			{
				ARPC_UINT32 __len__;
				if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
				value_.resize(__len__);
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					if(!__r__->readType(value_[i])) return false;
				}
			}
		}
		return true;
	}
	return false;
}
//=============================================================
static void initFuncEErrorCodeOfLogin(ARPC_EnumInfo* e)
{
		e->items_.push_back("EC_LG_InvalidUNPW");
		e->items_.push_back("EC_LG_TheSameAccountOnline");
}
ARPC_EnumInfo enumEErrorCodeOfLogin("EErrorCodeOfLogin", initFuncEErrorCodeOfLogin);
//=============================================================
COM_PlayerID::COM_PlayerID():
guid_(0)
{}
void COM_PlayerID::serialize(ARPC_ProtocolWriter* __s__) const
{
	// serialize guid_
	{
		__s__->writeType(guid_);
	}
	// serialize name_
	{
		__s__->writeType(name_);
	}
}
bool COM_PlayerID::deserialize(ARPC_ProtocolReader* __r__)
{
	// deserialize guid_
	{
		if(!__r__->readType(guid_)) return false;
	}
	// deserialize name_
	{
		if(!__r__->readType(name_, 65535)) return false;
	}
		return true;
}
bool COM_PlayerID::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_guid_:
		{
			// serialize guid_
			{
				__s__->writeType(guid_);
			}
		}
		return true;
		case FID_name_:
		{
			// serialize name_
			{
				__s__->writeType(name_);
			}
		}
		return true;
	}
	return false;
}
bool COM_PlayerID::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_guid_:
		{
			// deserialize guid_
			{
				if(!__r__->readType(guid_)) return false;
			}
		}
		return true;
		case FID_name_:
		{
			// deserialize name_
			{
				if(!__r__->readType(name_, 65535)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
COM_WeaponPackage::COM_WeaponPackage():
mainWeapon_(0)
,subWeapon_(0)
,meleeWeapon_(0)
,projectile0_(0)
,projectile1_(0)
,projectile2_(0)
{}
void COM_WeaponPackage::serialize(ARPC_ProtocolWriter* __s__) const
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	__fm__.writeBit((mainWeapon_==0)?false:true);
	__fm__.writeBit((subWeapon_==0)?false:true);
	__fm__.writeBit((meleeWeapon_==0)?false:true);
	__fm__.writeBit((projectile0_==0)?false:true);
	__fm__.writeBit((projectile1_==0)?false:true);
	__fm__.writeBit((projectile2_==0)?false:true);
	__s__->write(__fm__.masks_, 1);
	// serialize mainWeapon_
	{
		if(mainWeapon_ != 0){
		__s__->writeType(mainWeapon_);
		}
	}
	// serialize subWeapon_
	{
		if(subWeapon_ != 0){
		__s__->writeType(subWeapon_);
		}
	}
	// serialize meleeWeapon_
	{
		if(meleeWeapon_ != 0){
		__s__->writeType(meleeWeapon_);
		}
	}
	// serialize projectile0_
	{
		if(projectile0_ != 0){
		__s__->writeType(projectile0_);
		}
	}
	// serialize projectile1_
	{
		if(projectile1_ != 0){
		__s__->writeType(projectile1_);
		}
	}
	// serialize projectile2_
	{
		if(projectile2_ != 0){
		__s__->writeType(projectile2_);
		}
	}
}
bool COM_WeaponPackage::deserialize(ARPC_ProtocolReader* __r__)
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	if(!__r__->read(__fm__.masks_, 1)) return false;
	// deserialize mainWeapon_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(mainWeapon_)) return false;
		}
	}
	// deserialize subWeapon_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(subWeapon_)) return false;
		}
	}
	// deserialize meleeWeapon_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(meleeWeapon_)) return false;
		}
	}
	// deserialize projectile0_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(projectile0_)) return false;
		}
	}
	// deserialize projectile1_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(projectile1_)) return false;
		}
	}
	// deserialize projectile2_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(projectile2_)) return false;
		}
	}
		return true;
}
bool COM_WeaponPackage::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_mainWeapon_:
		{
			// serialize mainWeapon_
			{
				__s__->writeType(mainWeapon_);
			}
		}
		return true;
		case FID_subWeapon_:
		{
			// serialize subWeapon_
			{
				__s__->writeType(subWeapon_);
			}
		}
		return true;
		case FID_meleeWeapon_:
		{
			// serialize meleeWeapon_
			{
				__s__->writeType(meleeWeapon_);
			}
		}
		return true;
		case FID_projectile0_:
		{
			// serialize projectile0_
			{
				__s__->writeType(projectile0_);
			}
		}
		return true;
		case FID_projectile1_:
		{
			// serialize projectile1_
			{
				__s__->writeType(projectile1_);
			}
		}
		return true;
		case FID_projectile2_:
		{
			// serialize projectile2_
			{
				__s__->writeType(projectile2_);
			}
		}
		return true;
	}
	return false;
}
bool COM_WeaponPackage::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_mainWeapon_:
		{
			// deserialize mainWeapon_
			{
				if(!__r__->readType(mainWeapon_)) return false;
			}
		}
		return true;
		case FID_subWeapon_:
		{
			// deserialize subWeapon_
			{
				if(!__r__->readType(subWeapon_)) return false;
			}
		}
		return true;
		case FID_meleeWeapon_:
		{
			// deserialize meleeWeapon_
			{
				if(!__r__->readType(meleeWeapon_)) return false;
			}
		}
		return true;
		case FID_projectile0_:
		{
			// deserialize projectile0_
			{
				if(!__r__->readType(projectile0_)) return false;
			}
		}
		return true;
		case FID_projectile1_:
		{
			// deserialize projectile1_
			{
				if(!__r__->readType(projectile1_)) return false;
			}
		}
		return true;
		case FID_projectile2_:
		{
			// deserialize projectile2_
			{
				if(!__r__->readType(projectile2_)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
static void initFuncERoleType(ARPC_EnumInfo* e)
{
		e->items_.push_back("ERT_Female");
		e->items_.push_back("ERT_Male");
}
ARPC_EnumInfo enumERoleType("ERoleType", initFuncERoleType);
//=============================================================
COM_CreateRoleData::COM_CreateRoleData():
type_((ERoleType)(0))
{}
void COM_CreateRoleData::serialize(ARPC_ProtocolWriter* __s__) const
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	__fm__.writeBit(roleName_.length()?true:false);
	__fm__.writeBit((type_==(ERoleType)(0))?false:true);
	__s__->write(__fm__.masks_, 1);
	// serialize roleName_
	{
		if(roleName_.length()){
		__s__->writeType(roleName_);
		}
	}
	// serialize type_
	{
		ARPC_EnumSize __e__ = (ARPC_EnumSize)type_;
		if(__e__){
		__s__->writeType(__e__);
		}
	}
}
bool COM_CreateRoleData::deserialize(ARPC_ProtocolReader* __r__)
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	if(!__r__->read(__fm__.masks_, 1)) return false;
	// deserialize roleName_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(roleName_, 20)) return false;
		}
	}
	// deserialize type_
	{
		ARPC_EnumSize __e__ = 0;
		if(__fm__.readBit()){
		if(!__r__->readType(__e__) || __e__ >= 2) return false;
		type_ = (ERoleType)__e__;
		}
	}
		return true;
}
bool COM_CreateRoleData::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_roleName_:
		{
			// serialize roleName_
			{
				__s__->writeType(roleName_);
			}
		}
		return true;
		case FID_type_:
		{
			// serialize type_
			{
				ARPC_EnumSize __e__ = (ARPC_EnumSize)type_;
				__s__->writeType(__e__);
			}
		}
		return true;
	}
	return false;
}
bool COM_CreateRoleData::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_roleName_:
		{
			// deserialize roleName_
			{
				if(!__r__->readType(roleName_, 20)) return false;
			}
		}
		return true;
		case FID_type_:
		{
			// deserialize type_
			{
				ARPC_EnumSize __e__ = 0;
				if(!__r__->readType(__e__) || __e__ >= 2) return false;
				type_ = (ERoleType)__e__;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
COM_ItemInst::COM_ItemInst():
itemId_(0)
,instId_(0)
{}
void COM_ItemInst::serialize(ARPC_ProtocolWriter* __s__) const
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	__fm__.writeBit((itemId_==0)?false:true);
	__fm__.writeBit((instId_==0)?false:true);
	__s__->write(__fm__.masks_, 1);
	// serialize itemId_
	{
		if(itemId_ != 0){
		__s__->writeType(itemId_);
		}
	}
	// serialize instId_
	{
		if(instId_ != 0){
		__s__->writeType(instId_);
		}
	}
}
bool COM_ItemInst::deserialize(ARPC_ProtocolReader* __r__)
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	if(!__r__->read(__fm__.masks_, 1)) return false;
	// deserialize itemId_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(itemId_)) return false;
		}
	}
	// deserialize instId_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(instId_)) return false;
		}
	}
		return true;
}
bool COM_ItemInst::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_itemId_:
		{
			// serialize itemId_
			{
				__s__->writeType(itemId_);
			}
		}
		return true;
		case FID_instId_:
		{
			// serialize instId_
			{
				__s__->writeType(instId_);
			}
		}
		return true;
	}
	return false;
}
bool COM_ItemInst::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_itemId_:
		{
			// deserialize itemId_
			{
				if(!__r__->readType(itemId_)) return false;
			}
		}
		return true;
		case FID_instId_:
		{
			// deserialize instId_
			{
				if(!__r__->readType(instId_)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
COM_CarInst::COM_CarInst():
carId_(0)
,instId_(0)
{}
void COM_CarInst::serialize(ARPC_ProtocolWriter* __s__) const
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	__fm__.writeBit((carId_==0)?false:true);
	__fm__.writeBit((instId_==0)?false:true);
	__s__->write(__fm__.masks_, 1);
	// serialize carId_
	{
		if(carId_ != 0){
		__s__->writeType(carId_);
		}
	}
	// serialize instId_
	{
		if(instId_ != 0){
		__s__->writeType(instId_);
		}
	}
}
bool COM_CarInst::deserialize(ARPC_ProtocolReader* __r__)
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	if(!__r__->read(__fm__.masks_, 1)) return false;
	// deserialize carId_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(carId_)) return false;
		}
	}
	// deserialize instId_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(instId_)) return false;
		}
	}
		return true;
}
bool COM_CarInst::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_carId_:
		{
			// serialize carId_
			{
				__s__->writeType(carId_);
			}
		}
		return true;
		case FID_instId_:
		{
			// serialize instId_
			{
				__s__->writeType(instId_);
			}
		}
		return true;
	}
	return false;
}
bool COM_CarInst::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_carId_:
		{
			// deserialize carId_
			{
				if(!__r__->readType(carId_)) return false;
			}
		}
		return true;
		case FID_instId_:
		{
			// deserialize instId_
			{
				if(!__r__->readType(instId_)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
COM_RoomPlayerInfo::COM_RoomPlayerInfo():
userGuid_(0)
,type_((ERoleType)(0))
,level_(0)
,curCar_(0)
{}
void COM_RoomPlayerInfo::serialize(ARPC_ProtocolWriter* __s__) const
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	__fm__.writeBit((userGuid_==0)?false:true);
	__fm__.writeBit((type_==(ERoleType)(0))?false:true);
	__fm__.writeBit(roleName_.length()?true:false);
	__fm__.writeBit((level_==0)?false:true);
	__fm__.writeBit((curCar_==0)?false:true);
	__fm__.writeBit(curItems_.size()?true:false);
	__fm__.writeBit(storeCars_.size()?true:false);
	__fm__.writeBit(storeItems_.size()?true:false);
	__s__->write(__fm__.masks_, 1);
	// serialize userGuid_
	{
		if(userGuid_ != 0){
		__s__->writeType(userGuid_);
		}
	}
	// serialize type_
	{
		ARPC_EnumSize __e__ = (ARPC_EnumSize)type_;
		if(__e__){
		__s__->writeType(__e__);
		}
	}
	// serialize roleName_
	{
		if(roleName_.length()){
		__s__->writeType(roleName_);
		}
	}
	// serialize level_
	{
		if(level_ != 0){
		__s__->writeType(level_);
		}
	}
	// serialize curCar_
	{
		if(curCar_ != 0){
		__s__->writeType(curCar_);
		}
	}
	// serialize curItems_
	if(curItems_.size())
	{
		ARPC_UINT32 __len__ = (ARPC_UINT32)curItems_.size();
		__s__->writeDynSize(__len__); 
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			__s__->writeType(curItems_[i]);
		}
	}
	// serialize storeCars_
	if(storeCars_.size())
	{
		ARPC_UINT32 __len__ = (ARPC_UINT32)storeCars_.size();
		__s__->writeDynSize(__len__); 
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			storeCars_[i].serialize(__s__);
		}
	}
	// serialize storeItems_
	if(storeItems_.size())
	{
		ARPC_UINT32 __len__ = (ARPC_UINT32)storeItems_.size();
		__s__->writeDynSize(__len__); 
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			storeItems_[i].serialize(__s__);
		}
	}
}
bool COM_RoomPlayerInfo::deserialize(ARPC_ProtocolReader* __r__)
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	if(!__r__->read(__fm__.masks_, 1)) return false;
	// deserialize userGuid_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(userGuid_)) return false;
		}
	}
	// deserialize type_
	{
		ARPC_EnumSize __e__ = 0;
		if(__fm__.readBit()){
		if(!__r__->readType(__e__) || __e__ >= 2) return false;
		type_ = (ERoleType)__e__;
		}
	}
	// deserialize roleName_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(roleName_, 65535)) return false;
		}
	}
	// deserialize level_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(level_)) return false;
		}
	}
	// deserialize curCar_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(curCar_)) return false;
		}
	}
	// deserialize curItems_
	if(__fm__.readBit())
	{
		ARPC_UINT32 __len__;
		if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
		curItems_.resize(__len__);
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			if(!__r__->readType(curItems_[i])) return false;
		}
	}
	// deserialize storeCars_
	if(__fm__.readBit())
	{
		ARPC_UINT32 __len__;
		if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
		storeCars_.resize(__len__);
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			if(!storeCars_[i].deserialize(__r__)) return false;
		}
	}
	// deserialize storeItems_
	if(__fm__.readBit())
	{
		ARPC_UINT32 __len__;
		if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
		storeItems_.resize(__len__);
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			if(!storeItems_[i].deserialize(__r__)) return false;
		}
	}
		return true;
}
bool COM_RoomPlayerInfo::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_userGuid_:
		{
			// serialize userGuid_
			{
				__s__->writeType(userGuid_);
			}
		}
		return true;
		case FID_type_:
		{
			// serialize type_
			{
				ARPC_EnumSize __e__ = (ARPC_EnumSize)type_;
				__s__->writeType(__e__);
			}
		}
		return true;
		case FID_roleName_:
		{
			// serialize roleName_
			{
				__s__->writeType(roleName_);
			}
		}
		return true;
		case FID_level_:
		{
			// serialize level_
			{
				__s__->writeType(level_);
			}
		}
		return true;
		case FID_curCar_:
		{
			// serialize curCar_
			{
				__s__->writeType(curCar_);
			}
		}
		return true;
		case FID_curItems_:
		{
			// serialize curItems_
			{
				ARPC_UINT32 __len__ = (ARPC_UINT32)curItems_.size();
				__s__->writeDynSize(__len__); 
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					__s__->writeType(curItems_[i]);
				}
			}
		}
		return true;
		case FID_storeCars_:
		{
			// serialize storeCars_
			{
				ARPC_UINT32 __len__ = (ARPC_UINT32)storeCars_.size();
				__s__->writeDynSize(__len__); 
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					storeCars_[i].serialize(__s__);
				}
			}
		}
		return true;
		case FID_storeItems_:
		{
			// serialize storeItems_
			{
				ARPC_UINT32 __len__ = (ARPC_UINT32)storeItems_.size();
				__s__->writeDynSize(__len__); 
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					storeItems_[i].serialize(__s__);
				}
			}
		}
		return true;
	}
	return false;
}
bool COM_RoomPlayerInfo::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_userGuid_:
		{
			// deserialize userGuid_
			{
				if(!__r__->readType(userGuid_)) return false;
			}
		}
		return true;
		case FID_type_:
		{
			// deserialize type_
			{
				ARPC_EnumSize __e__ = 0;
				if(!__r__->readType(__e__) || __e__ >= 2) return false;
				type_ = (ERoleType)__e__;
			}
		}
		return true;
		case FID_roleName_:
		{
			// deserialize roleName_
			{
				if(!__r__->readType(roleName_, 65535)) return false;
			}
		}
		return true;
		case FID_level_:
		{
			// deserialize level_
			{
				if(!__r__->readType(level_)) return false;
			}
		}
		return true;
		case FID_curCar_:
		{
			// deserialize curCar_
			{
				if(!__r__->readType(curCar_)) return false;
			}
		}
		return true;
		case FID_curItems_:
		{
			// deserialize curItems_
			{
				ARPC_UINT32 __len__;
				if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
				curItems_.resize(__len__);
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					if(!__r__->readType(curItems_[i])) return false;
				}
			}
		}
		return true;
		case FID_storeCars_:
		{
			// deserialize storeCars_
			{
				ARPC_UINT32 __len__;
				if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
				storeCars_.resize(__len__);
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					if(!storeCars_[i].deserialize(__r__)) return false;
				}
			}
		}
		return true;
		case FID_storeItems_:
		{
			// deserialize storeItems_
			{
				ARPC_UINT32 __len__;
				if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
				storeItems_.resize(__len__);
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					if(!storeItems_[i].deserialize(__r__)) return false;
				}
			}
		}
		return true;
	}
	return false;
}
//=============================================================
COM_PlayerDetailData::COM_PlayerDetailData():
maxInstId_(0)
,birthday_(0)
,exp_(0)
,gold_(0)
,money_(0)
,gameNum_(0)
{}
void COM_PlayerDetailData::serialize(ARPC_ProtocolWriter* __s__) const
{
	COM_RoomPlayerInfo::serialize(__s__);
	//field mask
	ARPC_FieldMask<1> __fm__;
	__fm__.writeBit((maxInstId_==0)?false:true);
	__fm__.writeBit((birthday_==0)?false:true);
	__fm__.writeBit((exp_==0)?false:true);
	__fm__.writeBit((gold_==0)?false:true);
	__fm__.writeBit((money_==0)?false:true);
	__fm__.writeBit((gameNum_==0)?false:true);
	__s__->write(__fm__.masks_, 1);
	// serialize maxInstId_
	{
		if(maxInstId_ != 0){
		__s__->writeType(maxInstId_);
		}
	}
	// serialize birthday_
	{
		if(birthday_ != 0){
		__s__->writeType(birthday_);
		}
	}
	// serialize exp_
	{
		if(exp_ != 0){
		__s__->writeType(exp_);
		}
	}
	// serialize gold_
	{
		if(gold_ != 0){
		__s__->writeType(gold_);
		}
	}
	// serialize money_
	{
		if(money_ != 0){
		__s__->writeType(money_);
		}
	}
	// serialize gameNum_
	{
		if(gameNum_ != 0){
		__s__->writeType(gameNum_);
		}
	}
}
bool COM_PlayerDetailData::deserialize(ARPC_ProtocolReader* __r__)
{
	if(!COM_RoomPlayerInfo::deserialize(__r__)) return false;
	//field mask
	ARPC_FieldMask<1> __fm__;
	if(!__r__->read(__fm__.masks_, 1)) return false;
	// deserialize maxInstId_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(maxInstId_)) return false;
		}
	}
	// deserialize birthday_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(birthday_)) return false;
		}
	}
	// deserialize exp_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(exp_)) return false;
		}
	}
	// deserialize gold_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(gold_)) return false;
		}
	}
	// deserialize money_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(money_)) return false;
		}
	}
	// deserialize gameNum_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(gameNum_)) return false;
		}
	}
		return true;
}
bool COM_PlayerDetailData::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_maxInstId_:
		{
			// serialize maxInstId_
			{
				__s__->writeType(maxInstId_);
			}
		}
		return true;
		case FID_birthday_:
		{
			// serialize birthday_
			{
				__s__->writeType(birthday_);
			}
		}
		return true;
		case FID_exp_:
		{
			// serialize exp_
			{
				__s__->writeType(exp_);
			}
		}
		return true;
		case FID_gold_:
		{
			// serialize gold_
			{
				__s__->writeType(gold_);
			}
		}
		return true;
		case FID_money_:
		{
			// serialize money_
			{
				__s__->writeType(money_);
			}
		}
		return true;
		case FID_gameNum_:
		{
			// serialize gameNum_
			{
				__s__->writeType(gameNum_);
			}
		}
		return true;
	}
	return COM_RoomPlayerInfo::serializeField(fid, __s__);
}
bool COM_PlayerDetailData::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_maxInstId_:
		{
			// deserialize maxInstId_
			{
				if(!__r__->readType(maxInstId_)) return false;
			}
		}
		return true;
		case FID_birthday_:
		{
			// deserialize birthday_
			{
				if(!__r__->readType(birthday_)) return false;
			}
		}
		return true;
		case FID_exp_:
		{
			// deserialize exp_
			{
				if(!__r__->readType(exp_)) return false;
			}
		}
		return true;
		case FID_gold_:
		{
			// deserialize gold_
			{
				if(!__r__->readType(gold_)) return false;
			}
		}
		return true;
		case FID_money_:
		{
			// deserialize money_
			{
				if(!__r__->readType(money_)) return false;
			}
		}
		return true;
		case FID_gameNum_:
		{
			// deserialize gameNum_
			{
				if(!__r__->readType(gameNum_)) return false;
			}
		}
		return true;
	}
	return COM_RoomPlayerInfo::deserializeField(fid, __r__);
}
//=============================================================
static void initFuncEGameFunction(ARPC_EnumInfo* e)
{
		e->items_.push_back("EIF_None");
		e->items_.push_back("EIF_AvoidMissileAttck");
		e->items_.push_back("EIF_AvoidBanana");
}
ARPC_EnumInfo enumEGameFunction("EGameFunction", initFuncEGameFunction);
//=============================================================
COM_Car::COM_Car():
guid(0)
,buyGold(0)
,gameFunction((EGameFunction)(0))
{}
void COM_Car::serialize(ARPC_ProtocolWriter* __s__) const
{
	// serialize guid
	{
		__s__->writeType(guid);
	}
	// serialize carName
	{
		__s__->writeType(carName);
	}
	// serialize geometry
	{
		__s__->writeType(geometry);
	}
	// serialize texture
	{
		__s__->writeType(texture);
	}
	// serialize buyGold
	{
		__s__->writeType(buyGold);
	}
	// serialize gameFunction
	{
		ARPC_EnumSize __e__ = (ARPC_EnumSize)gameFunction;
		__s__->writeType(__e__);
	}
}
bool COM_Car::deserialize(ARPC_ProtocolReader* __r__)
{
	// deserialize guid
	{
		if(!__r__->readType(guid)) return false;
	}
	// deserialize carName
	{
		if(!__r__->readType(carName, 65535)) return false;
	}
	// deserialize geometry
	{
		if(!__r__->readType(geometry, 65535)) return false;
	}
	// deserialize texture
	{
		if(!__r__->readType(texture, 65535)) return false;
	}
	// deserialize buyGold
	{
		if(!__r__->readType(buyGold)) return false;
	}
	// deserialize gameFunction
	{
		ARPC_EnumSize __e__ = 0;
		if(!__r__->readType(__e__) || __e__ >= 3) return false;
		gameFunction = (EGameFunction)__e__;
	}
		return true;
}
bool COM_Car::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_guid:
		{
			// serialize guid
			{
				__s__->writeType(guid);
			}
		}
		return true;
		case FID_carName:
		{
			// serialize carName
			{
				__s__->writeType(carName);
			}
		}
		return true;
		case FID_geometry:
		{
			// serialize geometry
			{
				__s__->writeType(geometry);
			}
		}
		return true;
		case FID_texture:
		{
			// serialize texture
			{
				__s__->writeType(texture);
			}
		}
		return true;
		case FID_buyGold:
		{
			// serialize buyGold
			{
				__s__->writeType(buyGold);
			}
		}
		return true;
		case FID_gameFunction:
		{
			// serialize gameFunction
			{
				ARPC_EnumSize __e__ = (ARPC_EnumSize)gameFunction;
				__s__->writeType(__e__);
			}
		}
		return true;
	}
	return false;
}
bool COM_Car::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_guid:
		{
			// deserialize guid
			{
				if(!__r__->readType(guid)) return false;
			}
		}
		return true;
		case FID_carName:
		{
			// deserialize carName
			{
				if(!__r__->readType(carName, 65535)) return false;
			}
		}
		return true;
		case FID_geometry:
		{
			// deserialize geometry
			{
				if(!__r__->readType(geometry, 65535)) return false;
			}
		}
		return true;
		case FID_texture:
		{
			// deserialize texture
			{
				if(!__r__->readType(texture, 65535)) return false;
			}
		}
		return true;
		case FID_buyGold:
		{
			// deserialize buyGold
			{
				if(!__r__->readType(buyGold)) return false;
			}
		}
		return true;
		case FID_gameFunction:
		{
			// deserialize gameFunction
			{
				ARPC_EnumSize __e__ = 0;
				if(!__r__->readType(__e__) || __e__ >= 3) return false;
				gameFunction = (EGameFunction)__e__;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
static void initFuncEItemTime(ARPC_EnumInfo* e)
{
		e->items_.push_back("EIT_ForEver");
		e->items_.push_back("EIT_Time");
}
ARPC_EnumInfo enumEItemTime("EItemTime", initFuncEItemTime);
//=============================================================
static void initFuncEItemSlot(ARPC_EnumInfo* e)
{
		e->items_.push_back("EIS_HairSection");
		e->items_.push_back("EIS_HeadSection");
		e->items_.push_back("EIS_BodySection");
		e->items_.push_back("EIS_HandSection");
		e->items_.push_back("EIS_FeetSection");
		e->items_.push_back("EIS_HeadAttach");
		e->items_.push_back("EIS_BackAttach");
		e->items_.push_back("EIS_HandAttach");
		e->items_.push_back("EIS_MAX");
}
ARPC_EnumInfo enumEItemSlot("EItemSlot", initFuncEItemSlot);
//=============================================================
static void initFuncEItemType(ARPC_EnumInfo* e)
{
		e->items_.push_back("EEIT_Head");
		e->items_.push_back("EEIT_Hair");
		e->items_.push_back("EEIT_Clothes");
		e->items_.push_back("EEIT_Shoes");
		e->items_.push_back("EEIT_Glove");
		e->items_.push_back("EEIT_Glass");
		e->items_.push_back("EEIT_Wing");
		e->items_.push_back("EEIT_Cane");
		e->items_.push_back("EEIT_Pet");
		e->items_.push_back("EEIT_Balloon");
		e->items_.push_back("EEIT_Card");
		e->items_.push_back("EEIT_Paint");
		e->items_.push_back("EEIT_Box");
}
ARPC_EnumInfo enumEItemType("EItemType", initFuncEItemType);
//=============================================================
COM_Item::COM_Item():
guid(0)
,buyGold(0)
,rewardGold(0)
,rewardExp(0)
,stackMax(0)
,itemType((EItemType)(0))
,itemTime((EItemTime)(0))
,needRoleType((ERoleType)(0))
,gameFunction((EGameFunction)(0))
{}
void COM_Item::serialize(ARPC_ProtocolWriter* __s__) const
{
	// serialize guid
	{
		__s__->writeType(guid);
	}
	// serialize buyGold
	{
		__s__->writeType(buyGold);
	}
	// serialize rewardGold
	{
		__s__->writeType(rewardGold);
	}
	// serialize rewardExp
	{
		__s__->writeType(rewardExp);
	}
	// serialize stackMax
	{
		__s__->writeType(stackMax);
	}
	// serialize itemType
	{
		ARPC_EnumSize __e__ = (ARPC_EnumSize)itemType;
		__s__->writeType(__e__);
	}
	// serialize subAppName
	{
		__s__->writeType(subAppName);
	}
	// serialize icon
	{
		__s__->writeType(icon);
	}
	// serialize itemTime
	{
		ARPC_EnumSize __e__ = (ARPC_EnumSize)itemTime;
		__s__->writeType(__e__);
	}
	// serialize needRoleType
	{
		ARPC_EnumSize __e__ = (ARPC_EnumSize)needRoleType;
		__s__->writeType(__e__);
	}
	// serialize gameFunction
	{
		ARPC_EnumSize __e__ = (ARPC_EnumSize)gameFunction;
		__s__->writeType(__e__);
	}
	// serialize itemName
	{
		__s__->writeType(itemName);
	}
}
bool COM_Item::deserialize(ARPC_ProtocolReader* __r__)
{
	// deserialize guid
	{
		if(!__r__->readType(guid)) return false;
	}
	// deserialize buyGold
	{
		if(!__r__->readType(buyGold)) return false;
	}
	// deserialize rewardGold
	{
		if(!__r__->readType(rewardGold)) return false;
	}
	// deserialize rewardExp
	{
		if(!__r__->readType(rewardExp)) return false;
	}
	// deserialize stackMax
	{
		if(!__r__->readType(stackMax)) return false;
	}
	// deserialize itemType
	{
		ARPC_EnumSize __e__ = 0;
		if(!__r__->readType(__e__) || __e__ >= 13) return false;
		itemType = (EItemType)__e__;
	}
	// deserialize subAppName
	{
		if(!__r__->readType(subAppName, 65535)) return false;
	}
	// deserialize icon
	{
		if(!__r__->readType(icon, 65535)) return false;
	}
	// deserialize itemTime
	{
		ARPC_EnumSize __e__ = 0;
		if(!__r__->readType(__e__) || __e__ >= 2) return false;
		itemTime = (EItemTime)__e__;
	}
	// deserialize needRoleType
	{
		ARPC_EnumSize __e__ = 0;
		if(!__r__->readType(__e__) || __e__ >= 2) return false;
		needRoleType = (ERoleType)__e__;
	}
	// deserialize gameFunction
	{
		ARPC_EnumSize __e__ = 0;
		if(!__r__->readType(__e__) || __e__ >= 3) return false;
		gameFunction = (EGameFunction)__e__;
	}
	// deserialize itemName
	{
		if(!__r__->readType(itemName, 65535)) return false;
	}
		return true;
}
bool COM_Item::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_guid:
		{
			// serialize guid
			{
				__s__->writeType(guid);
			}
		}
		return true;
		case FID_buyGold:
		{
			// serialize buyGold
			{
				__s__->writeType(buyGold);
			}
		}
		return true;
		case FID_rewardGold:
		{
			// serialize rewardGold
			{
				__s__->writeType(rewardGold);
			}
		}
		return true;
		case FID_rewardExp:
		{
			// serialize rewardExp
			{
				__s__->writeType(rewardExp);
			}
		}
		return true;
		case FID_stackMax:
		{
			// serialize stackMax
			{
				__s__->writeType(stackMax);
			}
		}
		return true;
		case FID_itemType:
		{
			// serialize itemType
			{
				ARPC_EnumSize __e__ = (ARPC_EnumSize)itemType;
				__s__->writeType(__e__);
			}
		}
		return true;
		case FID_subAppName:
		{
			// serialize subAppName
			{
				__s__->writeType(subAppName);
			}
		}
		return true;
		case FID_icon:
		{
			// serialize icon
			{
				__s__->writeType(icon);
			}
		}
		return true;
		case FID_itemTime:
		{
			// serialize itemTime
			{
				ARPC_EnumSize __e__ = (ARPC_EnumSize)itemTime;
				__s__->writeType(__e__);
			}
		}
		return true;
		case FID_needRoleType:
		{
			// serialize needRoleType
			{
				ARPC_EnumSize __e__ = (ARPC_EnumSize)needRoleType;
				__s__->writeType(__e__);
			}
		}
		return true;
		case FID_gameFunction:
		{
			// serialize gameFunction
			{
				ARPC_EnumSize __e__ = (ARPC_EnumSize)gameFunction;
				__s__->writeType(__e__);
			}
		}
		return true;
		case FID_itemName:
		{
			// serialize itemName
			{
				__s__->writeType(itemName);
			}
		}
		return true;
	}
	return false;
}
bool COM_Item::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_guid:
		{
			// deserialize guid
			{
				if(!__r__->readType(guid)) return false;
			}
		}
		return true;
		case FID_buyGold:
		{
			// deserialize buyGold
			{
				if(!__r__->readType(buyGold)) return false;
			}
		}
		return true;
		case FID_rewardGold:
		{
			// deserialize rewardGold
			{
				if(!__r__->readType(rewardGold)) return false;
			}
		}
		return true;
		case FID_rewardExp:
		{
			// deserialize rewardExp
			{
				if(!__r__->readType(rewardExp)) return false;
			}
		}
		return true;
		case FID_stackMax:
		{
			// deserialize stackMax
			{
				if(!__r__->readType(stackMax)) return false;
			}
		}
		return true;
		case FID_itemType:
		{
			// deserialize itemType
			{
				ARPC_EnumSize __e__ = 0;
				if(!__r__->readType(__e__) || __e__ >= 13) return false;
				itemType = (EItemType)__e__;
			}
		}
		return true;
		case FID_subAppName:
		{
			// deserialize subAppName
			{
				if(!__r__->readType(subAppName, 65535)) return false;
			}
		}
		return true;
		case FID_icon:
		{
			// deserialize icon
			{
				if(!__r__->readType(icon, 65535)) return false;
			}
		}
		return true;
		case FID_itemTime:
		{
			// deserialize itemTime
			{
				ARPC_EnumSize __e__ = 0;
				if(!__r__->readType(__e__) || __e__ >= 2) return false;
				itemTime = (EItemTime)__e__;
			}
		}
		return true;
		case FID_needRoleType:
		{
			// deserialize needRoleType
			{
				ARPC_EnumSize __e__ = 0;
				if(!__r__->readType(__e__) || __e__ >= 2) return false;
				needRoleType = (ERoleType)__e__;
			}
		}
		return true;
		case FID_gameFunction:
		{
			// deserialize gameFunction
			{
				ARPC_EnumSize __e__ = 0;
				if(!__r__->readType(__e__) || __e__ >= 3) return false;
				gameFunction = (EGameFunction)__e__;
			}
		}
		return true;
		case FID_itemName:
		{
			// deserialize itemName
			{
				if(!__r__->readType(itemName, 65535)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
COM_DefaultRoleEquip::COM_DefaultRoleEquip():
hair(0)
,head(0)
,body(0)
,hand(0)
,feet(0)
{}
void COM_DefaultRoleEquip::serialize(ARPC_ProtocolWriter* __s__) const
{
	// serialize hair
	{
		__s__->writeType(hair);
	}
	// serialize head
	{
		__s__->writeType(head);
	}
	// serialize body
	{
		__s__->writeType(body);
	}
	// serialize hand
	{
		__s__->writeType(hand);
	}
	// serialize feet
	{
		__s__->writeType(feet);
	}
}
bool COM_DefaultRoleEquip::deserialize(ARPC_ProtocolReader* __r__)
{
	// deserialize hair
	{
		if(!__r__->readType(hair)) return false;
	}
	// deserialize head
	{
		if(!__r__->readType(head)) return false;
	}
	// deserialize body
	{
		if(!__r__->readType(body)) return false;
	}
	// deserialize hand
	{
		if(!__r__->readType(hand)) return false;
	}
	// deserialize feet
	{
		if(!__r__->readType(feet)) return false;
	}
		return true;
}
bool COM_DefaultRoleEquip::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_hair:
		{
			// serialize hair
			{
				__s__->writeType(hair);
			}
		}
		return true;
		case FID_head:
		{
			// serialize head
			{
				__s__->writeType(head);
			}
		}
		return true;
		case FID_body:
		{
			// serialize body
			{
				__s__->writeType(body);
			}
		}
		return true;
		case FID_hand:
		{
			// serialize hand
			{
				__s__->writeType(hand);
			}
		}
		return true;
		case FID_feet:
		{
			// serialize feet
			{
				__s__->writeType(feet);
			}
		}
		return true;
	}
	return false;
}
bool COM_DefaultRoleEquip::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_hair:
		{
			// deserialize hair
			{
				if(!__r__->readType(hair)) return false;
			}
		}
		return true;
		case FID_head:
		{
			// deserialize head
			{
				if(!__r__->readType(head)) return false;
			}
		}
		return true;
		case FID_body:
		{
			// deserialize body
			{
				if(!__r__->readType(body)) return false;
			}
		}
		return true;
		case FID_hand:
		{
			// deserialize hand
			{
				if(!__r__->readType(hand)) return false;
			}
		}
		return true;
		case FID_feet:
		{
			// deserialize feet
			{
				if(!__r__->readType(feet)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
void COM_Anim::serialize(ARPC_ProtocolWriter* __s__) const
{
	// serialize idle
	{
		__s__->writeType(idle);
	}
	// serialize run
	{
		__s__->writeType(run);
	}
}
bool COM_Anim::deserialize(ARPC_ProtocolReader* __r__)
{
	// deserialize idle
	{
		if(!__r__->readType(idle, 65535)) return false;
	}
	// deserialize run
	{
		if(!__r__->readType(run, 65535)) return false;
	}
		return true;
}
bool COM_Anim::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_idle:
		{
			// serialize idle
			{
				__s__->writeType(idle);
			}
		}
		return true;
		case FID_run:
		{
			// serialize run
			{
				__s__->writeType(run);
			}
		}
		return true;
	}
	return false;
}
bool COM_Anim::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_idle:
		{
			// deserialize idle
			{
				if(!__r__->readType(idle, 65535)) return false;
			}
		}
		return true;
		case FID_run:
		{
			// deserialize run
			{
				if(!__r__->readType(run, 65535)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
COM_Appearance::COM_Appearance():
guid(0)
{}
void COM_Appearance::serialize(ARPC_ProtocolWriter* __s__) const
{
	// serialize guid
	{
		__s__->writeType(guid);
	}
	// serialize appearanceName
	{
		__s__->writeType(appearanceName);
	}
	// serialize defaultRoleEquip
	{
		defaultRoleEquip.serialize(__s__);
	}
	// serialize anim
	{
		anim.serialize(__s__);
	}
}
bool COM_Appearance::deserialize(ARPC_ProtocolReader* __r__)
{
	// deserialize guid
	{
		if(!__r__->readType(guid)) return false;
	}
	// deserialize appearanceName
	{
		if(!__r__->readType(appearanceName, 65535)) return false;
	}
	// deserialize defaultRoleEquip
	{
		if(!defaultRoleEquip.deserialize(__r__)) return false;
	}
	// deserialize anim
	{
		if(!anim.deserialize(__r__)) return false;
	}
		return true;
}
bool COM_Appearance::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_guid:
		{
			// serialize guid
			{
				__s__->writeType(guid);
			}
		}
		return true;
		case FID_appearanceName:
		{
			// serialize appearanceName
			{
				__s__->writeType(appearanceName);
			}
		}
		return true;
		case FID_defaultRoleEquip:
		{
			// serialize defaultRoleEquip
			{
				defaultRoleEquip.serialize(__s__);
			}
		}
		return true;
		case FID_anim:
		{
			// serialize anim
			{
				anim.serialize(__s__);
			}
		}
		return true;
	}
	return false;
}
bool COM_Appearance::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_guid:
		{
			// deserialize guid
			{
				if(!__r__->readType(guid)) return false;
			}
		}
		return true;
		case FID_appearanceName:
		{
			// deserialize appearanceName
			{
				if(!__r__->readType(appearanceName, 65535)) return false;
			}
		}
		return true;
		case FID_defaultRoleEquip:
		{
			// deserialize defaultRoleEquip
			{
				if(!defaultRoleEquip.deserialize(__r__)) return false;
			}
		}
		return true;
		case FID_anim:
		{
			// deserialize anim
			{
				if(!anim.deserialize(__r__)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
COM_DefaultRole::COM_DefaultRole():
guid(0)
,roleAppearanceId(0)
,RoleType((ERoleType)(0))
,roleGold(0)
{}
void COM_DefaultRole::serialize(ARPC_ProtocolWriter* __s__) const
{
	// serialize guid
	{
		__s__->writeType(guid);
	}
	// serialize roleName
	{
		__s__->writeType(roleName);
	}
	// serialize roleAppearanceId
	{
		__s__->writeType(roleAppearanceId);
	}
	// serialize RoleType
	{
		ARPC_EnumSize __e__ = (ARPC_EnumSize)RoleType;
		__s__->writeType(__e__);
	}
	// serialize roleCar
	{
		ARPC_UINT32 __len__ = (ARPC_UINT32)roleCar.size();
		__s__->writeDynSize(__len__); 
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			__s__->writeType(roleCar[i]);
		}
	}
	// serialize roleStore
	{
		ARPC_UINT32 __len__ = (ARPC_UINT32)roleStore.size();
		__s__->writeDynSize(__len__); 
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			__s__->writeType(roleStore[i]);
		}
	}
	// serialize roleGold
	{
		__s__->writeType(roleGold);
	}
}
bool COM_DefaultRole::deserialize(ARPC_ProtocolReader* __r__)
{
	// deserialize guid
	{
		if(!__r__->readType(guid)) return false;
	}
	// deserialize roleName
	{
		if(!__r__->readType(roleName, 65535)) return false;
	}
	// deserialize roleAppearanceId
	{
		if(!__r__->readType(roleAppearanceId)) return false;
	}
	// deserialize RoleType
	{
		ARPC_EnumSize __e__ = 0;
		if(!__r__->readType(__e__) || __e__ >= 2) return false;
		RoleType = (ERoleType)__e__;
	}
	// deserialize roleCar
	{
		ARPC_UINT32 __len__;
		if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
		roleCar.resize(__len__);
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			if(!__r__->readType(roleCar[i])) return false;
		}
	}
	// deserialize roleStore
	{
		ARPC_UINT32 __len__;
		if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
		roleStore.resize(__len__);
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			if(!__r__->readType(roleStore[i])) return false;
		}
	}
	// deserialize roleGold
	{
		if(!__r__->readType(roleGold)) return false;
	}
		return true;
}
bool COM_DefaultRole::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_guid:
		{
			// serialize guid
			{
				__s__->writeType(guid);
			}
		}
		return true;
		case FID_roleName:
		{
			// serialize roleName
			{
				__s__->writeType(roleName);
			}
		}
		return true;
		case FID_roleAppearanceId:
		{
			// serialize roleAppearanceId
			{
				__s__->writeType(roleAppearanceId);
			}
		}
		return true;
		case FID_RoleType:
		{
			// serialize RoleType
			{
				ARPC_EnumSize __e__ = (ARPC_EnumSize)RoleType;
				__s__->writeType(__e__);
			}
		}
		return true;
		case FID_roleCar:
		{
			// serialize roleCar
			{
				ARPC_UINT32 __len__ = (ARPC_UINT32)roleCar.size();
				__s__->writeDynSize(__len__); 
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					__s__->writeType(roleCar[i]);
				}
			}
		}
		return true;
		case FID_roleStore:
		{
			// serialize roleStore
			{
				ARPC_UINT32 __len__ = (ARPC_UINT32)roleStore.size();
				__s__->writeDynSize(__len__); 
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					__s__->writeType(roleStore[i]);
				}
			}
		}
		return true;
		case FID_roleGold:
		{
			// serialize roleGold
			{
				__s__->writeType(roleGold);
			}
		}
		return true;
	}
	return false;
}
bool COM_DefaultRole::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_guid:
		{
			// deserialize guid
			{
				if(!__r__->readType(guid)) return false;
			}
		}
		return true;
		case FID_roleName:
		{
			// deserialize roleName
			{
				if(!__r__->readType(roleName, 65535)) return false;
			}
		}
		return true;
		case FID_roleAppearanceId:
		{
			// deserialize roleAppearanceId
			{
				if(!__r__->readType(roleAppearanceId)) return false;
			}
		}
		return true;
		case FID_RoleType:
		{
			// deserialize RoleType
			{
				ARPC_EnumSize __e__ = 0;
				if(!__r__->readType(__e__) || __e__ >= 2) return false;
				RoleType = (ERoleType)__e__;
			}
		}
		return true;
		case FID_roleCar:
		{
			// deserialize roleCar
			{
				ARPC_UINT32 __len__;
				if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
				roleCar.resize(__len__);
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					if(!__r__->readType(roleCar[i])) return false;
				}
			}
		}
		return true;
		case FID_roleStore:
		{
			// deserialize roleStore
			{
				ARPC_UINT32 __len__;
				if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
				roleStore.resize(__len__);
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					if(!__r__->readType(roleStore[i])) return false;
				}
			}
		}
		return true;
		case FID_roleGold:
		{
			// deserialize roleGold
			{
				if(!__r__->readType(roleGold)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
static void initFuncEMapType(ARPC_EnumInfo* e)
{
		e->items_.push_back("EMT_Random");
		e->items_.push_back("EMT_Forest");
		e->items_.push_back("EMT_City");
		e->items_.push_back("EMT_Ice");
		e->items_.push_back("EMT_Daemon");
		e->items_.push_back("EMT_Desert");
		e->items_.push_back("EMT_Cemetery");
}
ARPC_EnumInfo enumEMapType("EMapType", initFuncEMapType);
//=============================================================
COM_Map::COM_Map():
guid(0)
,mapType((EMapType)(0))
,maxLap(0)
,limitLevel(0)
,mapLevel(0)
{}
void COM_Map::serialize(ARPC_ProtocolWriter* __s__) const
{
	// serialize guid
	{
		__s__->writeType(guid);
	}
	// serialize mapName
	{
		__s__->writeType(mapName);
	}
	// serialize mapType
	{
		ARPC_EnumSize __e__ = (ARPC_EnumSize)mapType;
		__s__->writeType(__e__);
	}
	// serialize sceneName
	{
		__s__->writeType(sceneName);
	}
	// serialize maxLap
	{
		__s__->writeType(maxLap);
	}
	// serialize icon
	{
		__s__->writeType(icon);
	}
	// serialize limitLevel
	{
		__s__->writeType(limitLevel);
	}
	// serialize mapLevel
	{
		__s__->writeType(mapLevel);
	}
}
bool COM_Map::deserialize(ARPC_ProtocolReader* __r__)
{
	// deserialize guid
	{
		if(!__r__->readType(guid)) return false;
	}
	// deserialize mapName
	{
		if(!__r__->readType(mapName, 65535)) return false;
	}
	// deserialize mapType
	{
		ARPC_EnumSize __e__ = 0;
		if(!__r__->readType(__e__) || __e__ >= 7) return false;
		mapType = (EMapType)__e__;
	}
	// deserialize sceneName
	{
		if(!__r__->readType(sceneName, 65535)) return false;
	}
	// deserialize maxLap
	{
		if(!__r__->readType(maxLap)) return false;
	}
	// deserialize icon
	{
		if(!__r__->readType(icon, 65535)) return false;
	}
	// deserialize limitLevel
	{
		if(!__r__->readType(limitLevel)) return false;
	}
	// deserialize mapLevel
	{
		if(!__r__->readType(mapLevel)) return false;
	}
		return true;
}
bool COM_Map::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_guid:
		{
			// serialize guid
			{
				__s__->writeType(guid);
			}
		}
		return true;
		case FID_mapName:
		{
			// serialize mapName
			{
				__s__->writeType(mapName);
			}
		}
		return true;
		case FID_mapType:
		{
			// serialize mapType
			{
				ARPC_EnumSize __e__ = (ARPC_EnumSize)mapType;
				__s__->writeType(__e__);
			}
		}
		return true;
		case FID_sceneName:
		{
			// serialize sceneName
			{
				__s__->writeType(sceneName);
			}
		}
		return true;
		case FID_maxLap:
		{
			// serialize maxLap
			{
				__s__->writeType(maxLap);
			}
		}
		return true;
		case FID_icon:
		{
			// serialize icon
			{
				__s__->writeType(icon);
			}
		}
		return true;
		case FID_limitLevel:
		{
			// serialize limitLevel
			{
				__s__->writeType(limitLevel);
			}
		}
		return true;
		case FID_mapLevel:
		{
			// serialize mapLevel
			{
				__s__->writeType(mapLevel);
			}
		}
		return true;
	}
	return false;
}
bool COM_Map::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_guid:
		{
			// deserialize guid
			{
				if(!__r__->readType(guid)) return false;
			}
		}
		return true;
		case FID_mapName:
		{
			// deserialize mapName
			{
				if(!__r__->readType(mapName, 65535)) return false;
			}
		}
		return true;
		case FID_mapType:
		{
			// deserialize mapType
			{
				ARPC_EnumSize __e__ = 0;
				if(!__r__->readType(__e__) || __e__ >= 7) return false;
				mapType = (EMapType)__e__;
			}
		}
		return true;
		case FID_sceneName:
		{
			// deserialize sceneName
			{
				if(!__r__->readType(sceneName, 65535)) return false;
			}
		}
		return true;
		case FID_maxLap:
		{
			// deserialize maxLap
			{
				if(!__r__->readType(maxLap)) return false;
			}
		}
		return true;
		case FID_icon:
		{
			// deserialize icon
			{
				if(!__r__->readType(icon, 65535)) return false;
			}
		}
		return true;
		case FID_limitLevel:
		{
			// deserialize limitLevel
			{
				if(!__r__->readType(limitLevel)) return false;
			}
		}
		return true;
		case FID_mapLevel:
		{
			// deserialize mapLevel
			{
				if(!__r__->readType(mapLevel)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
COM_ShopItem::COM_ShopItem():
guid(0)
,itemId(0)
{}
void COM_ShopItem::serialize(ARPC_ProtocolWriter* __s__) const
{
	// serialize guid
	{
		__s__->writeType(guid);
	}
	// serialize itemId
	{
		__s__->writeType(itemId);
	}
}
bool COM_ShopItem::deserialize(ARPC_ProtocolReader* __r__)
{
	// deserialize guid
	{
		if(!__r__->readType(guid)) return false;
	}
	// deserialize itemId
	{
		if(!__r__->readType(itemId)) return false;
	}
		return true;
}
bool COM_ShopItem::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_guid:
		{
			// serialize guid
			{
				__s__->writeType(guid);
			}
		}
		return true;
		case FID_itemId:
		{
			// serialize itemId
			{
				__s__->writeType(itemId);
			}
		}
		return true;
	}
	return false;
}
bool COM_ShopItem::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_guid:
		{
			// deserialize guid
			{
				if(!__r__->readType(guid)) return false;
			}
		}
		return true;
		case FID_itemId:
		{
			// deserialize itemId
			{
				if(!__r__->readType(itemId)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
COM_ShopCar::COM_ShopCar():
guid(0)
,carId(0)
{}
void COM_ShopCar::serialize(ARPC_ProtocolWriter* __s__) const
{
	// serialize guid
	{
		__s__->writeType(guid);
	}
	// serialize carId
	{
		__s__->writeType(carId);
	}
}
bool COM_ShopCar::deserialize(ARPC_ProtocolReader* __r__)
{
	// deserialize guid
	{
		if(!__r__->readType(guid)) return false;
	}
	// deserialize carId
	{
		if(!__r__->readType(carId)) return false;
	}
		return true;
}
bool COM_ShopCar::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_guid:
		{
			// serialize guid
			{
				__s__->writeType(guid);
			}
		}
		return true;
		case FID_carId:
		{
			// serialize carId
			{
				__s__->writeType(carId);
			}
		}
		return true;
	}
	return false;
}
bool COM_ShopCar::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_guid:
		{
			// deserialize guid
			{
				if(!__r__->readType(guid)) return false;
			}
		}
		return true;
		case FID_carId:
		{
			// deserialize carId
			{
				if(!__r__->readType(carId)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
static void initFuncEGameType(ARPC_EnumInfo* e)
{
		e->items_.push_back("EGT_SingleItem");
		e->items_.push_back("EGT_MultiItem");
		e->items_.push_back("EGT_ItemGameEnd");
		e->items_.push_back("EGT_SingleSpeed");
		e->items_.push_back("EGT_MultiSpeed");
}
ARPC_EnumInfo enumEGameType("EGameType", initFuncEGameType);
//=============================================================
COM_CreateRoomInfo::COM_CreateRoomInfo():
gameType_((EGameType)(0))
,maxNum_(0)
{}
void COM_CreateRoomInfo::serialize(ARPC_ProtocolWriter* __s__) const
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	__fm__.writeBit(roomName_.length()?true:false);
	__fm__.writeBit((gameType_==(EGameType)(0))?false:true);
	__fm__.writeBit((maxNum_==0)?false:true);
	__s__->write(__fm__.masks_, 1);
	// serialize roomName_
	{
		if(roomName_.length()){
		__s__->writeType(roomName_);
		}
	}
	// serialize gameType_
	{
		ARPC_EnumSize __e__ = (ARPC_EnumSize)gameType_;
		if(__e__){
		__s__->writeType(__e__);
		}
	}
	// serialize maxNum_
	{
		if(maxNum_ != 0){
		__s__->writeType(maxNum_);
		}
	}
}
bool COM_CreateRoomInfo::deserialize(ARPC_ProtocolReader* __r__)
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	if(!__r__->read(__fm__.masks_, 1)) return false;
	// deserialize roomName_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(roomName_, 32)) return false;
		}
	}
	// deserialize gameType_
	{
		ARPC_EnumSize __e__ = 0;
		if(__fm__.readBit()){
		if(!__r__->readType(__e__) || __e__ >= 5) return false;
		gameType_ = (EGameType)__e__;
		}
	}
	// deserialize maxNum_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(maxNum_)) return false;
		}
	}
		return true;
}
bool COM_CreateRoomInfo::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_roomName_:
		{
			// serialize roomName_
			{
				__s__->writeType(roomName_);
			}
		}
		return true;
		case FID_gameType_:
		{
			// serialize gameType_
			{
				ARPC_EnumSize __e__ = (ARPC_EnumSize)gameType_;
				__s__->writeType(__e__);
			}
		}
		return true;
		case FID_maxNum_:
		{
			// serialize maxNum_
			{
				__s__->writeType(maxNum_);
			}
		}
		return true;
	}
	return false;
}
bool COM_CreateRoomInfo::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_roomName_:
		{
			// deserialize roomName_
			{
				if(!__r__->readType(roomName_, 32)) return false;
			}
		}
		return true;
		case FID_gameType_:
		{
			// deserialize gameType_
			{
				ARPC_EnumSize __e__ = 0;
				if(!__r__->readType(__e__) || __e__ >= 5) return false;
				gameType_ = (EGameType)__e__;
			}
		}
		return true;
		case FID_maxNum_:
		{
			// deserialize maxNum_
			{
				if(!__r__->readType(maxNum_)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
static void initFuncEGameItemType(ARPC_EnumInfo* e)
{
		e->items_.push_back("GIT_NULL");
		e->items_.push_back("GIT_Banana");
		e->items_.push_back("GIT_WaterFly");
		e->items_.push_back("GIT_WaterBall");
		e->items_.push_back("GIT_Shield");
		e->items_.push_back("GIT_Angel");
		e->items_.push_back("GIT_N2O");
		e->items_.push_back("GIT_Skull");
		e->items_.push_back("GIT_Cloud");
		e->items_.push_back("GIT_UFO");
		e->items_.push_back("GIT_Magnet");
		e->items_.push_back("GIT_Missile");
		e->items_.push_back("GIT_Max");
}
ARPC_EnumInfo enumEGameItemType("EGameItemType", initFuncEGameItemType);
//=============================================================
static void initFuncEErrorCodeOfRoomCreation(ARPC_EnumInfo* e)
{
		e->items_.push_back("EC_RC_Ok");
}
ARPC_EnumInfo enumEErrorCodeOfRoomCreation("EErrorCodeOfRoomCreation", initFuncEErrorCodeOfRoomCreation);
//=============================================================
static void initFuncEErrorCodeOfJoin(ARPC_EnumInfo* e)
{
		e->items_.push_back("EC_JR_Ok");
		e->items_.push_back("EC_JR_RoomIsClosed");
		e->items_.push_back("EC_JR_RoomIsFull");
		e->items_.push_back("EC_JR_IsPlaying");
}
ARPC_EnumInfo enumEErrorCodeOfJoin("EErrorCodeOfJoin", initFuncEErrorCodeOfJoin);
//=============================================================
static void initFuncERoomGameType(ARPC_EnumInfo* e)
{
		e->items_.push_back("RGT_Default");
		e->items_.push_back("RGT_Max");
}
ARPC_EnumInfo enumERoomGameType("ERoomGameType", initFuncERoomGameType);
//=============================================================
COM_RoomInfo::COM_RoomInfo():
guid_(0)
,curPlayerNum_(0)
,maxPlayerNum_(0)
,map_(0)
,type_((EGameType)(0))
,isPlaying_(false)
{}
void COM_RoomInfo::serialize(ARPC_ProtocolWriter* __s__) const
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	__fm__.writeBit((guid_==0)?false:true);
	__fm__.writeBit(true);
	__fm__.writeBit((curPlayerNum_==0)?false:true);
	__fm__.writeBit((maxPlayerNum_==0)?false:true);
	__fm__.writeBit(name_.length()?true:false);
	__fm__.writeBit((map_==0)?false:true);
	__fm__.writeBit((type_==(EGameType)(0))?false:true);
	__fm__.writeBit(isPlaying_);
	__s__->write(__fm__.masks_, 1);
	// serialize guid_
	{
		if(guid_ != 0){
		__s__->writeType(guid_);
		}
	}
	// serialize creator_
	{
		creator_.serialize(__s__);
	}
	// serialize curPlayerNum_
	{
		if(curPlayerNum_ != 0){
		__s__->writeType(curPlayerNum_);
		}
	}
	// serialize maxPlayerNum_
	{
		if(maxPlayerNum_ != 0){
		__s__->writeType(maxPlayerNum_);
		}
	}
	// serialize name_
	{
		if(name_.length()){
		__s__->writeType(name_);
		}
	}
	// serialize map_
	{
		if(map_ != 0){
		__s__->writeType(map_);
		}
	}
	// serialize type_
	{
		ARPC_EnumSize __e__ = (ARPC_EnumSize)type_;
		if(__e__){
		__s__->writeType(__e__);
		}
	}
	// serialize isPlaying_
	{
	}
}
bool COM_RoomInfo::deserialize(ARPC_ProtocolReader* __r__)
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	if(!__r__->read(__fm__.masks_, 1)) return false;
	// deserialize guid_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(guid_)) return false;
		}
	}
	// deserialize creator_
	{
		if(__fm__.readBit()){
		if(!creator_.deserialize(__r__)) return false;
		}
	}
	// deserialize curPlayerNum_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(curPlayerNum_)) return false;
		}
	}
	// deserialize maxPlayerNum_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(maxPlayerNum_)) return false;
		}
	}
	// deserialize name_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(name_, 65535)) return false;
		}
	}
	// deserialize map_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(map_)) return false;
		}
	}
	// deserialize type_
	{
		ARPC_EnumSize __e__ = 0;
		if(__fm__.readBit()){
		if(!__r__->readType(__e__) || __e__ >= 5) return false;
		type_ = (EGameType)__e__;
		}
	}
	// deserialize isPlaying_
	{
		isPlaying_ = __fm__.readBit();
	}
		return true;
}
bool COM_RoomInfo::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_guid_:
		{
			// serialize guid_
			{
				__s__->writeType(guid_);
			}
		}
		return true;
		case FID_creator_:
		{
			// serialize creator_
			{
				creator_.serialize(__s__);
			}
		}
		return true;
		case FID_curPlayerNum_:
		{
			// serialize curPlayerNum_
			{
				__s__->writeType(curPlayerNum_);
			}
		}
		return true;
		case FID_maxPlayerNum_:
		{
			// serialize maxPlayerNum_
			{
				__s__->writeType(maxPlayerNum_);
			}
		}
		return true;
		case FID_name_:
		{
			// serialize name_
			{
				__s__->writeType(name_);
			}
		}
		return true;
		case FID_map_:
		{
			// serialize map_
			{
				__s__->writeType(map_);
			}
		}
		return true;
		case FID_type_:
		{
			// serialize type_
			{
				ARPC_EnumSize __e__ = (ARPC_EnumSize)type_;
				__s__->writeType(__e__);
			}
		}
		return true;
		case FID_isPlaying_:
		{
			// serialize isPlaying_
			{
				__s__->writeType(isPlaying_);
			}
		}
		return true;
	}
	return false;
}
bool COM_RoomInfo::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_guid_:
		{
			// deserialize guid_
			{
				if(!__r__->readType(guid_)) return false;
			}
		}
		return true;
		case FID_creator_:
		{
			// deserialize creator_
			{
				if(!creator_.deserialize(__r__)) return false;
			}
		}
		return true;
		case FID_curPlayerNum_:
		{
			// deserialize curPlayerNum_
			{
				if(!__r__->readType(curPlayerNum_)) return false;
			}
		}
		return true;
		case FID_maxPlayerNum_:
		{
			// deserialize maxPlayerNum_
			{
				if(!__r__->readType(maxPlayerNum_)) return false;
			}
		}
		return true;
		case FID_name_:
		{
			// deserialize name_
			{
				if(!__r__->readType(name_, 65535)) return false;
			}
		}
		return true;
		case FID_map_:
		{
			// deserialize map_
			{
				if(!__r__->readType(map_)) return false;
			}
		}
		return true;
		case FID_type_:
		{
			// deserialize type_
			{
				ARPC_EnumSize __e__ = 0;
				if(!__r__->readType(__e__) || __e__ >= 5) return false;
				type_ = (EGameType)__e__;
			}
		}
		return true;
		case FID_isPlaying_:
		{
			// deserialize isPlaying_
			{
				if(!__r__->readType(isPlaying_)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
COM_GameItem::COM_GameItem():
itemId_(0)
,type((EGameItemType)(0))
{}
void COM_GameItem::serialize(ARPC_ProtocolWriter* __s__) const
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	__fm__.writeBit((itemId_==0)?false:true);
	__fm__.writeBit((type==(EGameItemType)(0))?false:true);
	__fm__.writeBit(itemPrefab.length()?true:false);
	__fm__.writeBit(name.length()?true:false);
	__fm__.writeBit(icon.length()?true:false);
	__s__->write(__fm__.masks_, 1);
	// serialize itemId_
	{
		if(itemId_ != 0){
		__s__->writeType(itemId_);
		}
	}
	// serialize type
	{
		ARPC_EnumSize __e__ = (ARPC_EnumSize)type;
		if(__e__){
		__s__->writeType(__e__);
		}
	}
	// serialize itemPrefab
	{
		if(itemPrefab.length()){
		__s__->writeType(itemPrefab);
		}
	}
	// serialize name
	{
		if(name.length()){
		__s__->writeType(name);
		}
	}
	// serialize icon
	{
		if(icon.length()){
		__s__->writeType(icon);
		}
	}
}
bool COM_GameItem::deserialize(ARPC_ProtocolReader* __r__)
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	if(!__r__->read(__fm__.masks_, 1)) return false;
	// deserialize itemId_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(itemId_)) return false;
		}
	}
	// deserialize type
	{
		ARPC_EnumSize __e__ = 0;
		if(__fm__.readBit()){
		if(!__r__->readType(__e__) || __e__ >= 13) return false;
		type = (EGameItemType)__e__;
		}
	}
	// deserialize itemPrefab
	{
		if(__fm__.readBit()){
		if(!__r__->readType(itemPrefab, 65535)) return false;
		}
	}
	// deserialize name
	{
		if(__fm__.readBit()){
		if(!__r__->readType(name, 65535)) return false;
		}
	}
	// deserialize icon
	{
		if(__fm__.readBit()){
		if(!__r__->readType(icon, 65535)) return false;
		}
	}
		return true;
}
bool COM_GameItem::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_itemId_:
		{
			// serialize itemId_
			{
				__s__->writeType(itemId_);
			}
		}
		return true;
		case FID_type:
		{
			// serialize type
			{
				ARPC_EnumSize __e__ = (ARPC_EnumSize)type;
				__s__->writeType(__e__);
			}
		}
		return true;
		case FID_itemPrefab:
		{
			// serialize itemPrefab
			{
				__s__->writeType(itemPrefab);
			}
		}
		return true;
		case FID_name:
		{
			// serialize name
			{
				__s__->writeType(name);
			}
		}
		return true;
		case FID_icon:
		{
			// serialize icon
			{
				__s__->writeType(icon);
			}
		}
		return true;
	}
	return false;
}
bool COM_GameItem::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_itemId_:
		{
			// deserialize itemId_
			{
				if(!__r__->readType(itemId_)) return false;
			}
		}
		return true;
		case FID_type:
		{
			// deserialize type
			{
				ARPC_EnumSize __e__ = 0;
				if(!__r__->readType(__e__) || __e__ >= 13) return false;
				type = (EGameItemType)__e__;
			}
		}
		return true;
		case FID_itemPrefab:
		{
			// deserialize itemPrefab
			{
				if(!__r__->readType(itemPrefab, 65535)) return false;
			}
		}
		return true;
		case FID_name:
		{
			// deserialize name
			{
				if(!__r__->readType(name, 65535)) return false;
			}
		}
		return true;
		case FID_icon:
		{
			// deserialize icon
			{
				if(!__r__->readType(icon, 65535)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
static void initFuncEMapRandomType(ARPC_EnumInfo* e)
{
		e->items_.push_back("MRT_VeryEasyRandom");
		e->items_.push_back("MRT_EasyRandom");
		e->items_.push_back("MRT_CommonRandom");
		e->items_.push_back("MRT_HardRandom");
		e->items_.push_back("MRT_Max");
}
ARPC_EnumInfo enumEMapRandomType("EMapRandomType", initFuncEMapRandomType);
//=============================================================
COM_RoomSetting::COM_RoomSetting():
map_(0)
,maxPlayerNum_(0)
{}
void COM_RoomSetting::serialize(ARPC_ProtocolWriter* __s__) const
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	__fm__.writeBit(name_.length()?true:false);
	__fm__.writeBit((map_==0)?false:true);
	__fm__.writeBit((maxPlayerNum_==0)?false:true);
	__s__->write(__fm__.masks_, 1);
	// serialize name_
	{
		if(name_.length()){
		__s__->writeType(name_);
		}
	}
	// serialize map_
	{
		if(map_ != 0){
		__s__->writeType(map_);
		}
	}
	// serialize maxPlayerNum_
	{
		if(maxPlayerNum_ != 0){
		__s__->writeType(maxPlayerNum_);
		}
	}
}
bool COM_RoomSetting::deserialize(ARPC_ProtocolReader* __r__)
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	if(!__r__->read(__fm__.masks_, 1)) return false;
	// deserialize name_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(name_, 65535)) return false;
		}
	}
	// deserialize map_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(map_)) return false;
		}
	}
	// deserialize maxPlayerNum_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(maxPlayerNum_)) return false;
		}
	}
		return true;
}
bool COM_RoomSetting::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_name_:
		{
			// serialize name_
			{
				__s__->writeType(name_);
			}
		}
		return true;
		case FID_map_:
		{
			// serialize map_
			{
				__s__->writeType(map_);
			}
		}
		return true;
		case FID_maxPlayerNum_:
		{
			// serialize maxPlayerNum_
			{
				__s__->writeType(maxPlayerNum_);
			}
		}
		return true;
	}
	return false;
}
bool COM_RoomSetting::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_name_:
		{
			// deserialize name_
			{
				if(!__r__->readType(name_, 65535)) return false;
			}
		}
		return true;
		case FID_map_:
		{
			// deserialize map_
			{
				if(!__r__->readType(map_)) return false;
			}
		}
		return true;
		case FID_maxPlayerNum_:
		{
			// deserialize maxPlayerNum_
			{
				if(!__r__->readType(maxPlayerNum_)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
static void initFuncEErrorCodeOfChangeCar(ARPC_EnumInfo* e)
{
		e->items_.push_back("EC_CR_NotFind");
}
ARPC_EnumInfo enumEErrorCodeOfChangeCar("EErrorCodeOfChangeCar", initFuncEErrorCodeOfChangeCar);
//=============================================================
static void initFuncEErrorCodeOfChangeEquip(ARPC_EnumInfo* e)
{
		e->items_.push_back("EC_CE_NotFind");
}
ARPC_EnumInfo enumEErrorCodeOfChangeEquip("EErrorCodeOfChangeEquip", initFuncEErrorCodeOfChangeEquip);
//=============================================================
static void initFuncEErrorCodeOfBuyItem(ARPC_EnumInfo* e)
{
		e->items_.push_back("EC_BI_None");
		e->items_.push_back("EC_BI_NoEnoughMoney");
}
ARPC_EnumInfo enumEErrorCodeOfBuyItem("EErrorCodeOfBuyItem", initFuncEErrorCodeOfBuyItem);
//=============================================================
static void initFuncEStuffType(ARPC_EnumInfo* e)
{
		e->items_.push_back("ST_Item");
		e->items_.push_back("ST_Car");
}
ARPC_EnumInfo enumEStuffType("EStuffType", initFuncEStuffType);
//=============================================================
COM_RoomSyncData::COM_RoomSyncData():
guid_(0)
{}
void COM_RoomSyncData::serialize(ARPC_ProtocolWriter* __s__) const
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	__fm__.writeBit((guid_==0)?false:true);
	__fm__.writeBit(true);
	__fm__.writeBit(true);
	__fm__.writeBit(players_.size()?true:false);
	__fm__.writeBit(readyPlayers_.size()?true:false);
	__s__->write(__fm__.masks_, 1);
	// serialize guid_
	{
		if(guid_ != 0){
		__s__->writeType(guid_);
		}
	}
	// serialize creator_
	{
		creator_.serialize(__s__);
	}
	// serialize setting_
	{
		setting_.serialize(__s__);
	}
	// serialize players_
	if(players_.size())
	{
		ARPC_UINT32 __len__ = (ARPC_UINT32)players_.size();
		__s__->writeDynSize(__len__); 
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			players_[i].serialize(__s__);
		}
	}
	// serialize readyPlayers_
	if(readyPlayers_.size())
	{
		ARPC_UINT32 __len__ = (ARPC_UINT32)readyPlayers_.size();
		__s__->writeDynSize(__len__); 
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			__s__->writeType(readyPlayers_[i]);
		}
	}
}
bool COM_RoomSyncData::deserialize(ARPC_ProtocolReader* __r__)
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	if(!__r__->read(__fm__.masks_, 1)) return false;
	// deserialize guid_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(guid_)) return false;
		}
	}
	// deserialize creator_
	{
		if(__fm__.readBit()){
		if(!creator_.deserialize(__r__)) return false;
		}
	}
	// deserialize setting_
	{
		if(__fm__.readBit()){
		if(!setting_.deserialize(__r__)) return false;
		}
	}
	// deserialize players_
	if(__fm__.readBit())
	{
		ARPC_UINT32 __len__;
		if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
		players_.resize(__len__);
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			if(!players_[i].deserialize(__r__)) return false;
		}
	}
	// deserialize readyPlayers_
	if(__fm__.readBit())
	{
		ARPC_UINT32 __len__;
		if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
		readyPlayers_.resize(__len__);
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			if(!__r__->readType(readyPlayers_[i])) return false;
		}
	}
		return true;
}
bool COM_RoomSyncData::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_guid_:
		{
			// serialize guid_
			{
				__s__->writeType(guid_);
			}
		}
		return true;
		case FID_creator_:
		{
			// serialize creator_
			{
				creator_.serialize(__s__);
			}
		}
		return true;
		case FID_setting_:
		{
			// serialize setting_
			{
				setting_.serialize(__s__);
			}
		}
		return true;
		case FID_players_:
		{
			// serialize players_
			{
				ARPC_UINT32 __len__ = (ARPC_UINT32)players_.size();
				__s__->writeDynSize(__len__); 
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					players_[i].serialize(__s__);
				}
			}
		}
		return true;
		case FID_readyPlayers_:
		{
			// serialize readyPlayers_
			{
				ARPC_UINT32 __len__ = (ARPC_UINT32)readyPlayers_.size();
				__s__->writeDynSize(__len__); 
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					__s__->writeType(readyPlayers_[i]);
				}
			}
		}
		return true;
	}
	return false;
}
bool COM_RoomSyncData::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_guid_:
		{
			// deserialize guid_
			{
				if(!__r__->readType(guid_)) return false;
			}
		}
		return true;
		case FID_creator_:
		{
			// deserialize creator_
			{
				if(!creator_.deserialize(__r__)) return false;
			}
		}
		return true;
		case FID_setting_:
		{
			// deserialize setting_
			{
				if(!setting_.deserialize(__r__)) return false;
			}
		}
		return true;
		case FID_players_:
		{
			// deserialize players_
			{
				ARPC_UINT32 __len__;
				if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
				players_.resize(__len__);
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					if(!players_[i].deserialize(__r__)) return false;
				}
			}
		}
		return true;
		case FID_readyPlayers_:
		{
			// deserialize readyPlayers_
			{
				ARPC_UINT32 __len__;
				if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
				readyPlayers_.resize(__len__);
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					if(!__r__->readType(readyPlayers_[i])) return false;
				}
			}
		}
		return true;
	}
	return false;
}
//=============================================================
static void initFuncEPCMovementDir(ARPC_EnumInfo* e)
{
		e->items_.push_back("PCMD_None");
		e->items_.push_back("PCMD_F");
		e->items_.push_back("PCMD_FR");
		e->items_.push_back("PCMD_R");
		e->items_.push_back("PCMD_BR");
		e->items_.push_back("PCMD_B");
		e->items_.push_back("PCMD_BL");
		e->items_.push_back("PCMD_L");
		e->items_.push_back("PCMD_FL");
}
ARPC_EnumInfo enumEPCMovementDir("EPCMovementDir", initFuncEPCMovementDir);
//=============================================================
COM_Vector3::COM_Vector3():
x_(0)
,y_(0)
,z_(0)
{}
void COM_Vector3::serialize(ARPC_ProtocolWriter* __s__) const
{
	// serialize x_
	{
		__s__->writeType(x_);
	}
	// serialize y_
	{
		__s__->writeType(y_);
	}
	// serialize z_
	{
		__s__->writeType(z_);
	}
}
bool COM_Vector3::deserialize(ARPC_ProtocolReader* __r__)
{
	// deserialize x_
	{
		if(!__r__->readType(x_)) return false;
	}
	// deserialize y_
	{
		if(!__r__->readType(y_)) return false;
	}
	// deserialize z_
	{
		if(!__r__->readType(z_)) return false;
	}
		return true;
}
bool COM_Vector3::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_x_:
		{
			// serialize x_
			{
				__s__->writeType(x_);
			}
		}
		return true;
		case FID_y_:
		{
			// serialize y_
			{
				__s__->writeType(y_);
			}
		}
		return true;
		case FID_z_:
		{
			// serialize z_
			{
				__s__->writeType(z_);
			}
		}
		return true;
	}
	return false;
}
bool COM_Vector3::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_x_:
		{
			// deserialize x_
			{
				if(!__r__->readType(x_)) return false;
			}
		}
		return true;
		case FID_y_:
		{
			// deserialize y_
			{
				if(!__r__->readType(y_)) return false;
			}
		}
		return true;
		case FID_z_:
		{
			// deserialize z_
			{
				if(!__r__->readType(z_)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
COM_PCMovementState::COM_PCMovementState():
time_(0)
{}
void COM_PCMovementState::serialize(ARPC_ProtocolWriter* __s__) const
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	__fm__.writeBit((time_==0)?false:true);
	__fm__.writeBit(true);
	__fm__.writeBit(true);
	__fm__.writeBit(true);
	__fm__.writeBit(true);
	__fm__.writeBit(true);
	__s__->write(__fm__.masks_, 1);
	// serialize time_
	{
		if(time_ != 0){
		__s__->writeType(time_);
		}
	}
	// serialize pos_
	{
		pos_.serialize(__s__);
	}
	// serialize linVel_
	{
		linVel_.serialize(__s__);
	}
	// serialize linAcc_
	{
		linAcc_.serialize(__s__);
	}
	// serialize eulerAngles_
	{
		eulerAngles_.serialize(__s__);
	}
	// serialize rotVel_
	{
		rotVel_.serialize(__s__);
	}
}
bool COM_PCMovementState::deserialize(ARPC_ProtocolReader* __r__)
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	if(!__r__->read(__fm__.masks_, 1)) return false;
	// deserialize time_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(time_)) return false;
		}
	}
	// deserialize pos_
	{
		if(__fm__.readBit()){
		if(!pos_.deserialize(__r__)) return false;
		}
	}
	// deserialize linVel_
	{
		if(__fm__.readBit()){
		if(!linVel_.deserialize(__r__)) return false;
		}
	}
	// deserialize linAcc_
	{
		if(__fm__.readBit()){
		if(!linAcc_.deserialize(__r__)) return false;
		}
	}
	// deserialize eulerAngles_
	{
		if(__fm__.readBit()){
		if(!eulerAngles_.deserialize(__r__)) return false;
		}
	}
	// deserialize rotVel_
	{
		if(__fm__.readBit()){
		if(!rotVel_.deserialize(__r__)) return false;
		}
	}
		return true;
}
bool COM_PCMovementState::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_time_:
		{
			// serialize time_
			{
				__s__->writeType(time_);
			}
		}
		return true;
		case FID_pos_:
		{
			// serialize pos_
			{
				pos_.serialize(__s__);
			}
		}
		return true;
		case FID_linVel_:
		{
			// serialize linVel_
			{
				linVel_.serialize(__s__);
			}
		}
		return true;
		case FID_linAcc_:
		{
			// serialize linAcc_
			{
				linAcc_.serialize(__s__);
			}
		}
		return true;
		case FID_eulerAngles_:
		{
			// serialize eulerAngles_
			{
				eulerAngles_.serialize(__s__);
			}
		}
		return true;
		case FID_rotVel_:
		{
			// serialize rotVel_
			{
				rotVel_.serialize(__s__);
			}
		}
		return true;
	}
	return false;
}
bool COM_PCMovementState::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_time_:
		{
			// deserialize time_
			{
				if(!__r__->readType(time_)) return false;
			}
		}
		return true;
		case FID_pos_:
		{
			// deserialize pos_
			{
				if(!pos_.deserialize(__r__)) return false;
			}
		}
		return true;
		case FID_linVel_:
		{
			// deserialize linVel_
			{
				if(!linVel_.deserialize(__r__)) return false;
			}
		}
		return true;
		case FID_linAcc_:
		{
			// deserialize linAcc_
			{
				if(!linAcc_.deserialize(__r__)) return false;
			}
		}
		return true;
		case FID_eulerAngles_:
		{
			// deserialize eulerAngles_
			{
				if(!eulerAngles_.deserialize(__r__)) return false;
			}
		}
		return true;
		case FID_rotVel_:
		{
			// deserialize rotVel_
			{
				if(!rotVel_.deserialize(__r__)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
COM_PCSyncData::COM_PCSyncData():
guid_(0)
{}
void COM_PCSyncData::serialize(ARPC_ProtocolWriter* __s__) const
{
	// serialize guid_
	{
		__s__->writeType(guid_);
	}
	// serialize name_
	{
		__s__->writeType(name_);
	}
	// serialize ms_
	{
		ms_.serialize(__s__);
	}
}
bool COM_PCSyncData::deserialize(ARPC_ProtocolReader* __r__)
{
	// deserialize guid_
	{
		if(!__r__->readType(guid_)) return false;
	}
	// deserialize name_
	{
		if(!__r__->readType(name_, 65535)) return false;
	}
	// deserialize ms_
	{
		if(!ms_.deserialize(__r__)) return false;
	}
		return true;
}
bool COM_PCSyncData::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_guid_:
		{
			// serialize guid_
			{
				__s__->writeType(guid_);
			}
		}
		return true;
		case FID_name_:
		{
			// serialize name_
			{
				__s__->writeType(name_);
			}
		}
		return true;
		case FID_ms_:
		{
			// serialize ms_
			{
				ms_.serialize(__s__);
			}
		}
		return true;
	}
	return false;
}
bool COM_PCSyncData::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_guid_:
		{
			// deserialize guid_
			{
				if(!__r__->readType(guid_)) return false;
			}
		}
		return true;
		case FID_name_:
		{
			// deserialize name_
			{
				if(!__r__->readType(name_, 65535)) return false;
			}
		}
		return true;
		case FID_ms_:
		{
			// deserialize ms_
			{
				if(!ms_.deserialize(__r__)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
void COM_GameSyncData::serialize(ARPC_ProtocolWriter* __s__) const
{
	// serialize mapName_
	{
		__s__->writeType(mapName_);
	}
	// serialize players_
	{
		ARPC_UINT32 __len__ = (ARPC_UINT32)players_.size();
		__s__->writeDynSize(__len__); 
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			players_[i].serialize(__s__);
		}
	}
}
bool COM_GameSyncData::deserialize(ARPC_ProtocolReader* __r__)
{
	// deserialize mapName_
	{
		if(!__r__->readType(mapName_, 65535)) return false;
	}
	// deserialize players_
	{
		ARPC_UINT32 __len__;
		if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
		players_.resize(__len__);
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			if(!players_[i].deserialize(__r__)) return false;
		}
	}
		return true;
}
bool COM_GameSyncData::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_mapName_:
		{
			// serialize mapName_
			{
				__s__->writeType(mapName_);
			}
		}
		return true;
		case FID_players_:
		{
			// serialize players_
			{
				ARPC_UINT32 __len__ = (ARPC_UINT32)players_.size();
				__s__->writeDynSize(__len__); 
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					players_[i].serialize(__s__);
				}
			}
		}
		return true;
	}
	return false;
}
bool COM_GameSyncData::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_mapName_:
		{
			// deserialize mapName_
			{
				if(!__r__->readType(mapName_, 65535)) return false;
			}
		}
		return true;
		case FID_players_:
		{
			// deserialize players_
			{
				ARPC_UINT32 __len__;
				if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
				players_.resize(__len__);
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					if(!players_[i].deserialize(__r__)) return false;
				}
			}
		}
		return true;
	}
	return false;
}
//=============================================================
static void initFuncEStartGameFailType(ARPC_EnumInfo* e)
{
		e->items_.push_back("ESGF_NoAllReady");
		e->items_.push_back("ESGF_NoEnoughNum");
		e->items_.push_back("ESGF_InvalidMap");
}
ARPC_EnumInfo enumEStartGameFailType("EStartGameFailType", initFuncEStartGameFailType);
//=============================================================
COM_ClZonePlayer::COM_ClZonePlayer():
userId_(0)
,startPos_(0)
,group_(0)
{}
void COM_ClZonePlayer::serialize(ARPC_ProtocolWriter* __s__) const
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	__fm__.writeBit((userId_==0)?false:true);
	__fm__.writeBit((startPos_==0)?false:true);
	__fm__.writeBit((group_==0)?false:true);
	__s__->write(__fm__.masks_, 1);
	// serialize userId_
	{
		if(userId_ != 0){
		__s__->writeType(userId_);
		}
	}
	// serialize startPos_
	{
		if(startPos_ != 0){
		__s__->writeType(startPos_);
		}
	}
	// serialize group_
	{
		if(group_ != 0){
		__s__->writeType(group_);
		}
	}
}
bool COM_ClZonePlayer::deserialize(ARPC_ProtocolReader* __r__)
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	if(!__r__->read(__fm__.masks_, 1)) return false;
	// deserialize userId_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(userId_)) return false;
		}
	}
	// deserialize startPos_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(startPos_)) return false;
		}
	}
	// deserialize group_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(group_)) return false;
		}
	}
		return true;
}
bool COM_ClZonePlayer::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_userId_:
		{
			// serialize userId_
			{
				__s__->writeType(userId_);
			}
		}
		return true;
		case FID_startPos_:
		{
			// serialize startPos_
			{
				__s__->writeType(startPos_);
			}
		}
		return true;
		case FID_group_:
		{
			// serialize group_
			{
				__s__->writeType(group_);
			}
		}
		return true;
	}
	return false;
}
bool COM_ClZonePlayer::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_userId_:
		{
			// deserialize userId_
			{
				if(!__r__->readType(userId_)) return false;
			}
		}
		return true;
		case FID_startPos_:
		{
			// deserialize startPos_
			{
				if(!__r__->readType(startPos_)) return false;
			}
		}
		return true;
		case FID_group_:
		{
			// deserialize group_
			{
				if(!__r__->readType(group_)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
COM_InitClZoneData::COM_InitClZoneData():
roomId_(0)
,mapId_(0)
,gametype_((EGameType)(0))
{}
void COM_InitClZoneData::serialize(ARPC_ProtocolWriter* __s__) const
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	__fm__.writeBit((roomId_==0)?false:true);
	__fm__.writeBit((mapId_==0)?false:true);
	__fm__.writeBit((gametype_==(EGameType)(0))?false:true);
	__fm__.writeBit(players_.size()?true:false);
	__s__->write(__fm__.masks_, 1);
	// serialize roomId_
	{
		if(roomId_ != 0){
		__s__->writeType(roomId_);
		}
	}
	// serialize mapId_
	{
		if(mapId_ != 0){
		__s__->writeType(mapId_);
		}
	}
	// serialize gametype_
	{
		ARPC_EnumSize __e__ = (ARPC_EnumSize)gametype_;
		if(__e__){
		__s__->writeType(__e__);
		}
	}
	// serialize players_
	if(players_.size())
	{
		ARPC_UINT32 __len__ = (ARPC_UINT32)players_.size();
		__s__->writeDynSize(__len__); 
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			players_[i].serialize(__s__);
		}
	}
}
bool COM_InitClZoneData::deserialize(ARPC_ProtocolReader* __r__)
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	if(!__r__->read(__fm__.masks_, 1)) return false;
	// deserialize roomId_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(roomId_)) return false;
		}
	}
	// deserialize mapId_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(mapId_)) return false;
		}
	}
	// deserialize gametype_
	{
		ARPC_EnumSize __e__ = 0;
		if(__fm__.readBit()){
		if(!__r__->readType(__e__) || __e__ >= 5) return false;
		gametype_ = (EGameType)__e__;
		}
	}
	// deserialize players_
	if(__fm__.readBit())
	{
		ARPC_UINT32 __len__;
		if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
		players_.resize(__len__);
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			if(!players_[i].deserialize(__r__)) return false;
		}
	}
		return true;
}
bool COM_InitClZoneData::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_roomId_:
		{
			// serialize roomId_
			{
				__s__->writeType(roomId_);
			}
		}
		return true;
		case FID_mapId_:
		{
			// serialize mapId_
			{
				__s__->writeType(mapId_);
			}
		}
		return true;
		case FID_gametype_:
		{
			// serialize gametype_
			{
				ARPC_EnumSize __e__ = (ARPC_EnumSize)gametype_;
				__s__->writeType(__e__);
			}
		}
		return true;
		case FID_players_:
		{
			// serialize players_
			{
				ARPC_UINT32 __len__ = (ARPC_UINT32)players_.size();
				__s__->writeDynSize(__len__); 
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					players_[i].serialize(__s__);
				}
			}
		}
		return true;
	}
	return false;
}
bool COM_InitClZoneData::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_roomId_:
		{
			// deserialize roomId_
			{
				if(!__r__->readType(roomId_)) return false;
			}
		}
		return true;
		case FID_mapId_:
		{
			// deserialize mapId_
			{
				if(!__r__->readType(mapId_)) return false;
			}
		}
		return true;
		case FID_gametype_:
		{
			// deserialize gametype_
			{
				ARPC_EnumSize __e__ = 0;
				if(!__r__->readType(__e__) || __e__ >= 5) return false;
				gametype_ = (EGameType)__e__;
			}
		}
		return true;
		case FID_players_:
		{
			// deserialize players_
			{
				ARPC_UINT32 __len__;
				if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
				players_.resize(__len__);
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					if(!players_[i].deserialize(__r__)) return false;
				}
			}
		}
		return true;
	}
	return false;
}
//=============================================================
COM_PlayerGameResult::COM_PlayerGameResult():
userGuid_(0)
,time_(0)
,rewardExp_(0)
,rewardGold_(0)
{}
void COM_PlayerGameResult::serialize(ARPC_ProtocolWriter* __s__) const
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	__fm__.writeBit((userGuid_==0)?false:true);
	__fm__.writeBit((time_==0)?false:true);
	__fm__.writeBit((rewardExp_==0)?false:true);
	__fm__.writeBit((rewardGold_==0)?false:true);
	__s__->write(__fm__.masks_, 1);
	// serialize userGuid_
	{
		if(userGuid_ != 0){
		__s__->writeType(userGuid_);
		}
	}
	// serialize time_
	{
		if(time_ != 0){
		__s__->writeType(time_);
		}
	}
	// serialize rewardExp_
	{
		if(rewardExp_ != 0){
		__s__->writeType(rewardExp_);
		}
	}
	// serialize rewardGold_
	{
		if(rewardGold_ != 0){
		__s__->writeType(rewardGold_);
		}
	}
}
bool COM_PlayerGameResult::deserialize(ARPC_ProtocolReader* __r__)
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	if(!__r__->read(__fm__.masks_, 1)) return false;
	// deserialize userGuid_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(userGuid_)) return false;
		}
	}
	// deserialize time_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(time_)) return false;
		}
	}
	// deserialize rewardExp_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(rewardExp_)) return false;
		}
	}
	// deserialize rewardGold_
	{
		if(__fm__.readBit()){
		if(!__r__->readType(rewardGold_)) return false;
		}
	}
		return true;
}
bool COM_PlayerGameResult::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_userGuid_:
		{
			// serialize userGuid_
			{
				__s__->writeType(userGuid_);
			}
		}
		return true;
		case FID_time_:
		{
			// serialize time_
			{
				__s__->writeType(time_);
			}
		}
		return true;
		case FID_rewardExp_:
		{
			// serialize rewardExp_
			{
				__s__->writeType(rewardExp_);
			}
		}
		return true;
		case FID_rewardGold_:
		{
			// serialize rewardGold_
			{
				__s__->writeType(rewardGold_);
			}
		}
		return true;
	}
	return false;
}
bool COM_PlayerGameResult::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_userGuid_:
		{
			// deserialize userGuid_
			{
				if(!__r__->readType(userGuid_)) return false;
			}
		}
		return true;
		case FID_time_:
		{
			// deserialize time_
			{
				if(!__r__->readType(time_)) return false;
			}
		}
		return true;
		case FID_rewardExp_:
		{
			// deserialize rewardExp_
			{
				if(!__r__->readType(rewardExp_)) return false;
			}
		}
		return true;
		case FID_rewardGold_:
		{
			// deserialize rewardGold_
			{
				if(!__r__->readType(rewardGold_)) return false;
			}
		}
		return true;
	}
	return false;
}
//=============================================================
void COM_GameResult::serialize(ARPC_ProtocolWriter* __s__) const
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	__fm__.writeBit(gameResult_.size()?true:false);
	__s__->write(__fm__.masks_, 1);
	// serialize gameResult_
	if(gameResult_.size())
	{
		ARPC_UINT32 __len__ = (ARPC_UINT32)gameResult_.size();
		__s__->writeDynSize(__len__); 
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			gameResult_[i].serialize(__s__);
		}
	}
}
bool COM_GameResult::deserialize(ARPC_ProtocolReader* __r__)
{
	//field mask
	ARPC_FieldMask<1> __fm__;
	if(!__r__->read(__fm__.masks_, 1)) return false;
	// deserialize gameResult_
	if(__fm__.readBit())
	{
		ARPC_UINT32 __len__;
		if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
		gameResult_.resize(__len__);
		for(ARPC_UINT32 i = 0; i < __len__; i++)
		{
			if(!gameResult_[i].deserialize(__r__)) return false;
		}
	}
		return true;
}
bool COM_GameResult::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_gameResult_:
		{
			// serialize gameResult_
			{
				ARPC_UINT32 __len__ = (ARPC_UINT32)gameResult_.size();
				__s__->writeDynSize(__len__); 
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					gameResult_[i].serialize(__s__);
				}
			}
		}
		return true;
	}
	return false;
}
bool COM_GameResult::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_gameResult_:
		{
			// deserialize gameResult_
			{
				ARPC_UINT32 __len__;
				if(!__r__->readDynSize(__len__) || __len__ > 65535) return false;
				gameResult_.resize(__len__);
				for(ARPC_UINT32 i = 0; i < __len__; i++)
				{
					if(!gameResult_[i].deserialize(__r__)) return false;
				}
			}
		}
		return true;
	}
	return false;
}
//=============================================================
COM_Vector2::COM_Vector2():
x_(0)
,y_(0)
{}
void COM_Vector2::serialize(ARPC_ProtocolWriter* __s__) const
{
	// serialize x_
	{
		__s__->writeType(x_);
	}
	// serialize y_
	{
		__s__->writeType(y_);
	}
}
bool COM_Vector2::deserialize(ARPC_ProtocolReader* __r__)
{
	// deserialize x_
	{
		if(!__r__->readType(x_)) return false;
	}
	// deserialize y_
	{
		if(!__r__->readType(y_)) return false;
	}
		return true;
}
bool COM_Vector2::serializeField(ARPC_UINT32 fid, ARPC_ProtocolWriter* __s__) const
{
	switch(fid)
	{
		case FID_x_:
		{
			// serialize x_
			{
				__s__->writeType(x_);
			}
		}
		return true;
		case FID_y_:
		{
			// serialize y_
			{
				__s__->writeType(y_);
			}
		}
		return true;
	}
	return false;
}
bool COM_Vector2::deserializeField(ARPC_UINT32 fid, ARPC_ProtocolReader* __r__)
{
	switch(fid)
	{
		case FID_x_:
		{
			// deserialize x_
			{
				if(!__r__->readType(x_)) return false;
			}
		}
		return true;
		case FID_y_:
		{
			// deserialize y_
			{
				if(!__r__->readType(y_)) return false;
			}
		}
		return true;
	}
	return false;
}
