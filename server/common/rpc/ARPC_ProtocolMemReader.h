//==============================================================================
/**
 * @file		ARPC_ProtocolMemReader.h
 *
 * @date		2010:2:26
 *
 * @author		Lucifer<hotlala8088@gmail.com>
 *
 * Copyright (C) 2004-2010 Lucifer. All Rights Reserved.
 */
//==============================================================================
#ifndef __ARPC_ProtocolMemReader_h__
#define __ARPC_ProtocolMemReader_h__

#include "ARPC_ProtocolReader.h"
#include <string.h>

/** 从一个指定内存区读取数据的读取器实现.
*/
class ARPC_ProtocolMemReader : public ARPC_ProtocolReader
{
public:
	ARPC_ProtocolMemReader(void* b, size_t l):
	buffer_((char*)b),
	length_(l),
	rdptr_(0)
	{}

	virtual bool read(void* data, size_t len)
	{
		if(length_ < rdptr_ + len)
			return false;
		::memcpy(data, buffer_ + rdptr_, len);
		rdptr_ += len;
		return true;
	}

private:
	char*			buffer_;
	size_t			length_;
	size_t			rdptr_;
};


#endif//__ARPC_ProtocolMemReader_h__