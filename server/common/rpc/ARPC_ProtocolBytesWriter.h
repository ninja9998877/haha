//==============================================================================
/**
 * @file		ARPC_ProtocolBytesWriter.h
 *
 * @date		2010:2:26
 *
 * @author		Lucifer<hotlala8088@gmail.com>
 *
 * Copyright (C) 2004-2010 Lucifer. All Rights Reserved.
 */
//==============================================================================
#ifndef __ARPC_ProtocolBytesWriter_h__
#define __ARPC_ProtocolBytesWriter_h__

#include "ARPC_ProtocolWriter.h"
#include <string.h>
#include <vector>

/** 写入一个bytes数组. */
class ARPC_ProtocolBytesWriter : public ARPC_ProtocolWriter
{
public:
	ARPC_ProtocolBytesWriter(std::vector<ARPC_UINT8>& b):
	bytes_(b)
	{}

	virtual void write(const void* data, size_t len)
	{
		size_t s = bytes_.size();
		bytes_.resize(s + len);
		::memcpy(&(bytes_[s]), data, len);
	}
	std::vector<ARPC_UINT8>& getBytes()	{ return bytes_; }

private:
	std::vector<ARPC_UINT8>& bytes_;
};


#endif//__ARPC_ProtocolBytesWriter_h__