//==============================================================================
/**
 * @file		ARPC_ProtocolWriter.h
 *
 * @date		2010:2:26
 *
 * @author		Lucifer<hotlala8088@gmail.com>
 *
 * Copyright (C) 2004-2010 Lucifer. All Rights Reserved.
 */
//==============================================================================
#ifndef __ARPC_ProtocolWriter_h__
#define __ARPC_ProtocolWriter_h__

#include "ARPC_Pre.h"
#include <string>

/** ARPC通讯协议写入器接口.
	ARPC_ProtocolWriter 为生成的service stub提供arpc协议整编数据写入接口。
	一个 ARPC_ProtocolWriter 可以得到 一个service stub 在序列化rpc数据后的写入事件，
	派生类可以重载这个接口，将数据写入到内存或直接写入网络。
*/
class ARPC_ProtocolWriter
{
public:
	/** 写入rpc序列化数据. 
		stub在rpc调用过程中通过此接口写入序列化数据.
		@param data 数据指针.
		@param len 数据长度.
	*/
	virtual void write(const void* data, size_t len) = 0;

	/** @name write basic types. */
	//@{
	void writeType(ARPC_INT64 v)
	{
		write(&v, sizeof(ARPC_INT64));
	}
	void writeType(ARPC_UINT64 v)
	{
		write(&v, sizeof(ARPC_UINT64));
	}
	void writeType(ARPC_DOUBLE v)
	{
		write(&v, sizeof(ARPC_DOUBLE));
	}
	void writeType(ARPC_FLOAT v)
	{
		write(&v, sizeof(ARPC_FLOAT));
	}
	void writeType(ARPC_INT32 v)
	{
		write(&v, sizeof(ARPC_INT32));
	}
	void writeType(ARPC_UINT32 v)
	{
		write(&v, sizeof(ARPC_UINT32));
	}
	void writeType(ARPC_INT16 v)
	{
		write(&v, sizeof(ARPC_INT16));
	}
	void writeType(ARPC_UINT16 v)
	{
		write(&v, sizeof(ARPC_UINT16));
	}
	void writeType(ARPC_INT8 v)
	{
		write(&v, sizeof(ARPC_INT8));
	}
	void writeType(ARPC_UINT8 v)
	{
		write(&v, sizeof(ARPC_UINT8));
	}
	void writeType(ARPC_BOOL v)
	{
		char vv = v?1:0;
		write(&vv, sizeof(ARPC_BOOL));
	}
	void writeType(const ARPC_STRING& v)
	{
		ARPC_UINT32 len = (ARPC_UINT32)v.length();
		writeDynSize(len);
		write(v.c_str(), v.length());
	}
	void writeDynSize(ARPC_UINT32 s)
	{
		ARPC_UINT8* p = (ARPC_UINT8*)(&s);
		ARPC_UINT8 n = 0;
		if(s <= 0X3F)
			n = 0;
		else if(s <= 0X3FFF)
			n = 1;
		else if(s <= 0X3FFFFF)
			n = 2;
		else if(s <= 0X3FFFFFFF)
			n = 3;
		p[n] |= (n<<6);
		for(int i = (int)n; i >= 0; i--)
			writeType(p[i]);
	}
	//@}
};



#endif//__ARPC_ProtocolWriter_h__