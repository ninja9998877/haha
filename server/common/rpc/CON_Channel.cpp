//==============================================================================
// CON_Channel.cpp Lucifer<hotlala8088@gmail.com> 2010:3:5
//==============================================================================
#if defined (_MSC_VER)
	// Disable vc warnings.
	#define _CRT_SECURE_NO_WARNINGS
	#pragma warning(disable:4996)
	#pragma warning(disable:4267)
	#pragma warning(disable:4129)//unrecognized character escape sequence
	#pragma warning(disable:4819)//The file contains a character that cannot be represented in the current code page (936). Save the file in Unicode format to prevent data loss
#endif

#include "CON_Channel.h"
#include "CON_ChannelConnection.h"

CON_Channel::CON_Channel():
guid_(0),
conn_(NULL)
{
}

CON_Channel::~CON_Channel()
{
	// 如果没有关闭，关闭之。
	close();
}

bool CON_Channel::isValid()
{
	return (conn_ == NULL)?false:true;
}

void CON_Channel::close()
{
	if(conn_)
		conn_->finiChannelConnData(this);
}

void CON_Channel::initSendingData()
{
	if(conn_)
		conn_->initChannelSendingData(this);
}

void CON_Channel::fillSendingData(void* data, size_t size)
{
	if(conn_)
		conn_->fillSendingData(data, size);
}

void CON_Channel::flushSendingData()
{
	if(conn_)
		conn_->flushSendingData();
}
