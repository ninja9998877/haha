//==============================================================================
/**
 * @file		CON_ARPCConnection.h
 *
 * @date		2010:2:26
 *
 * @author		Lucifer<hotlala8088@gmail.com>
 *
 * Copyright (C) 2004-2010 Lucifer. All Rights Reserved.
 */
//==============================================================================
#ifndef __CON_ARPCConnection_h__
#define __CON_ARPCConnection_h__


#include "ARPC_ProtocolMemReader.h"
#include "ARPC_ProtocolWriter.h"

#include "CON_Connection.h"

/** 基于arpc协议的网络通讯连接.
	CON_ARPCConnection 基于aprc协议进行连接双方的通讯.
	CON_ARPCConnection 派生自 一个指定的 arpc stub，所以可以直接调用arpc stub 的
	接口向对端发送数据.
	CON_ARPCConnection 还保存一个 arpc proxy 的引用，当接收到对方数据后，会调用这
	个 proxy 的接口来 处理数据.

	arpc 协议格式:
	- HdrType: 数据长度.
	- arpc数据
*/
template< class ARPC_STUB, class ARPC_PROXY >
class CON_ARPCConnection : 
	public ARPC_ProtocolWriter,
	public CON_Connection,
	public ARPC_STUB
{
public:
	typedef unsigned short	HdrType;

	CON_ARPCConnection(size_t rbSize = 0XFFFF, size_t sbSize = 0XFFFF):
	CON_Connection(rbSize, sbSize),
	proxy_(NULL),
	hdr_(NULL)
	{
	}

	/** 设置proxy指针. */
	void setProxy(ARPC_PROXY* p)		{ proxy_ = p; }

	/** 处理连接接收到的数据.
		handleReceived 将接收到的数据交给arpc proxy进行dispatch，调用相应的函数.
	*/
	virtual int handleReceived(void* data, size_t size)
	{
		if(!proxy_)
			return size;

		// 解析发送过来的协议，并进行arpc dispatch，调用相应的处理函数。 
		int processed = 0;

		// 循环处理，直到不能处理为止.
		unsigned char* d = (unsigned char*)data;
		size_t dLeft = size;
		while(1)
		{
			// 检测数据头长度.
			if(sizeof(HdrType) > dLeft)
				return processed;
			HdrType dLen = *((HdrType*)d);

			// 检查数据完整性.
			size_t dTLen = sizeof(HdrType) + dLen;
			if(dTLen > dLeft)
				return processed;

			// 一个完整消息被接受 交给arpc dispatcher处理.
			ARPC_ProtocolMemReader r((d+sizeof(HdrType)), dLen);
			if(!proxy_->dispatch(&r))
				return -1;

			d			+= dTLen;
			dLeft		-= dTLen;
			processed	+= dTLen;
		}
		return processed;
	}

	/** Stub events. */
	virtual ARPC_ProtocolWriter* methodBegin()
	{
		if(getStatus() != CON_Connection::Established)
			return NULL;

		// 保存头指针位置.
		hdr_ = sendBuf_.wr_ptr();
		// 填入一个临时的头部数据.
		HdrType tempHdr = 0;
		fillSendBuffer(&tempHdr, sizeof(HdrType));
		return this;
	}
	virtual void methodEnd()
	{
		if(getStatus() != CON_Connection::Established)
			return;
		// 重置消息头
		*((HdrType*)hdr_) = (HdrType)((sendBuf_.wr_ptr() - hdr_) - sizeof(HdrType));
		// 发送aprc数据.
		flushSendBuffer();
	}
	/** ARPC_ProtocolWriter interface. */
	virtual void write(const void* data, size_t len)
	{
		if(getStatus() != CON_Connection::Established)
			return;
		fillSendBuffer((void*)data, len);
	}

private:
	ARPC_PROXY*			proxy_;
	char*				hdr_;
};

#endif//__CON_ARPCConnection_h__
