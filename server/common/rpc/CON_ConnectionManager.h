//==============================================================================
/**
 * @file		CON_ConnectionManager.h
 *
 * @date		2011/5/9	13:48
 *
 * @author		Lucifer<hotlala8088@gmail.com>
 *
 * Copyright (C) 2004-2011 Lucifer. All Rights Reserved.
 */
//==============================================================================
#ifndef __CON_ConnectionManager_h__
#define __CON_ConnectionManager_h__

#include "CON_Connection.h"
#include <vector>

template<class CONN>
class CON_ConnectionManager
{
public:
	typedef std::vector<CONN*> ConnectionList;

	CON_ConnectionManager() 
	{
	}

	virtual ~CON_ConnectionManager() 
	{
		destroyAllConnections(); 
	}

	/** 销毁所有无效的连接. */
	void destroyInvalidConnections()
	{
		for(typename ConnectionList::iterator iter = connections_.begin(); iter != connections_.end();)
		{
			if(!checkConnection(*iter))
			{
				delete *iter;
				iter = connections_.erase(iter);
			}
			else
				++iter;
		}
	}

	/** 销毁所有的连接. */
	void destroyAllConnections()
	{
		for(typename ConnectionList::iterator iter = connections_.begin(); iter != connections_.end(); ++iter)
			delete *iter;
		connections_.clear();
	}

protected:
	/** 检查一个连接的有效性. 
		此函数默认只对连接本身的状态进行检测，派生类可以重载此函数实现自定义的检
		查方法.
		@return true 表示有效.
	*/
	virtual bool checkConnection(CONN* c)
	{
		return (c->getStatus() == CON_Connection::Established);
	}

	ConnectionList connections_;
};

#endif//__CON_ConnectionManager_h__
