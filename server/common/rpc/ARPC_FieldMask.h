//==============================================================================
/**
 * @file		ARPC_FieldMask.h
 *
 * @date		2010:4:26
 *
 * @author		Lucifer<hotlala8088@gmail.com>
 *
 * Copyright (C) 2004-2010 Lucifer. All Rights Reserved.
 */
//==============================================================================
#ifndef __ARPC_FieldMask_h__
#define __ARPC_FieldMask_h__

#include "ARPC_Pre.h"

/** 用于设置和读取按照bit排列的field mask. */
template<int LEN>
class ARPC_FieldMask
{
public:
	ARPC_FieldMask():
	pos_(0)
	{
		for( int i = 0; i < LEN; i++ )
			masks_[i] = 0;
	}

	void writeBit( bool b )
	{
		if(b)
			masks_[pos_>>3] |= (128>>(pos_&0X00000007));
		pos_++;
	}

	bool readBit()
	{
		bool r = masks_[pos_>>3] & (128>>(pos_&0X00000007))?true:false;
		pos_++;
		return r;
	}

	unsigned char	masks_[LEN];
	unsigned int	pos_;
};

#endif//__ARPC_FieldMask_h__
