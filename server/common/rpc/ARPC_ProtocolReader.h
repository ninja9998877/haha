//==============================================================================
/**
 * @file		ARPC_ProtocolReader.h
 *
 * @date		2010:2:26
 *
 * @author		Lucifer<hotlala8088@gmail.com>
 *
 * Copyright (C) 2004-2010 Lucifer. All Rights Reserved.
 */
//==============================================================================
#ifndef __ARPC_ProtocolReader_h__
#define __ARPC_ProtocolReader_h__

#include "ARPC_Pre.h"
#include <string>

/** ARPC通讯协议读取器接口.
	ARPC_ProtocolReader 为 service proxy 提供arpc通讯协议解编数据读取接口。
	一个 ARPC_ProtocolReader 可以得到 一个service proxy 在反序列化rpc数据后的读取事件，
	派生类可以重载这个接口，将数据从内存或网络读取出来。
*/
class ARPC_ProtocolReader
{
public:
	/** 读入rpc序列化数据. 
		proxy在rpc调用过程中通过此接口读取序列化数据.
		@param data 数据指针.
		@param len 数据长度.
	*/
	virtual bool read(void* data, size_t len) = 0;

	/** @name read basic types. */
	//@{
	bool readType(ARPC_INT64& v)
	{
		return read(&v, sizeof(ARPC_INT64));
	}
	bool readType(ARPC_UINT64& v)
	{
		return read(&v, sizeof(ARPC_UINT64));
	}
	bool readType(ARPC_DOUBLE& v)
	{
		return read(&v, sizeof(ARPC_DOUBLE));
	}
	bool readType(ARPC_FLOAT& v)
	{
		return read(&v, sizeof(ARPC_FLOAT));
	}
	bool readType(ARPC_INT32& v)
	{
		return read(&v, sizeof(ARPC_INT32));
	}
	bool readType(ARPC_UINT32& v)
	{
		return read(&v, sizeof(ARPC_UINT32));
	}
	bool readType(ARPC_INT16& v)
	{
		return read(&v, sizeof(ARPC_INT16));
	}
	bool readType(ARPC_UINT16& v)
	{
		return read(&v, sizeof(ARPC_UINT16));
	}
	bool readType(ARPC_INT8& v)
	{
		return read(&v, sizeof(ARPC_INT8));
	}
	bool readType(ARPC_UINT8& v)
	{
		return read(&v, sizeof(ARPC_UINT8));
	}
	bool readType(ARPC_BOOL& v)
	{
		char vv;
		if(!read(&vv, sizeof(ARPC_BOOL)))
			return false;
		v = vv?true:false;
		return true;
	}
	bool readType(ARPC_STRING& v, ARPC_UINT32 maxlen)
	{
		ARPC_UINT32 len;
		if(!readDynSize(len) || len > maxlen)
			return false;
		v.resize(len);
		return read((void*)v.c_str(), len);
	}
	bool readDynSize(ARPC_UINT32& s)
	{
		s = 0;
		ARPC_UINT8 b;
		if(!readType(b))
			return false;
		size_t n = (b & 0XC0)>>6;
		s = (b & 0X3F);
		for(size_t i = 0; i < n; i++)
		{
			if(!readType(b))
				return false;
			s = (s<<8)|b;
		}
		return true;
	}
	//@}
};


#endif//__ARPC_ProtocolReader_h__