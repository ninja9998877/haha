//==============================================================================
/**
 * @file		CON_ARPCChannel.h
 *
 * @date		2010:3:1
 *
 * @author		Lucifer<hotlala8088@gmail.com>
 *
 * Copyright (C) 2004-2010 Lucifer. All Rights Reserved.
 */
//==============================================================================
#ifndef __CON_ARPCChannel_h__
#define __CON_ARPCChannel_h__

#include "ARPC_ProtocolMemReader.h"
#include "ARPC_ProtocolWriter.h"

#include "CON_Channel.h"

/** 基于arpc协议的网络通讯 CON_Channel.

	CON_ARPCChannel 将一个 ARPC_STUB，一个 ARPC_PROXY 与一个 CON_Channel 通信管道
	整合到一起，实现了使用这个 CON_Channel 对 ARPC借口调用的发送和接收工作。
	CON_ARPCChannel 将接收到的数据通过 ARPC_PROXY 进行消息分派，调用 ARPC_PROXY 
	中对应的 ARPC 接口函数。使用者可以通过重载 ARPC_STUB 中的接口对消息进行处理。
	使用者还可以通过调用这个 CON_ARPCChannel 的 ARPC_STUB 接口发送 ARPC 消息。

	@see CON_ARPCConnection
*/
template< class ARPC_STUB, class ARPC_PROXY >
class CON_ARPCChannel :
	public CON_Channel,
	public ARPC_STUB,
	public ARPC_ProtocolWriter /* protocol writer for ARPC_STUB */
{
public:
	/** 从这个channel接收到的数据会交给 p 进行分派处理. */
	CON_ARPCChannel():
	proxy_(NULL)
	{
	}

	/** 设置proxy指针. */
	void setProxy(ARPC_PROXY* p)		{ proxy_ = p; }

	/** 处理channel接收到的数据.
		交给arpc proxy进行dispatch.
	*/
	virtual bool handleReceived(void* data, size_t size)
	{
		if(!proxy_)
			return true;

		// 将接收到的消息通过 ARPC_PROXY::dispatch 进行分派，调用对应的接口。
		ARPC_ProtocolMemReader r(data, size);
		return proxy_->dispatch(&r);
	}

	/** Stub events. */
	virtual ARPC_ProtocolWriter* methodBegin()
	{
		initSendingData();
		return this;
	}
	virtual void methodEnd()
	{
		flushSendingData();
	}

	/** ARPC_ProtocolWriter interface. */
	virtual void write(const void* data, size_t len)
	{
		fillSendingData((void*)data, len);
	}

protected:
	ARPC_PROXY*		proxy_;
};

#endif//__CON_ARPCChannel_h__
