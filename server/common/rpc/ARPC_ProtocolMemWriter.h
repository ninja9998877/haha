//==============================================================================
/**
 * @file		ARPC_ProtocolMemWriter.h
 *
 * @date		2010:2:26
 *
 * @author		Lucifer<hotlala8088@gmail.com>
 *
 * Copyright (C) 2004-2010 Lucifer. All Rights Reserved.
 */
//==============================================================================
#ifndef __ARPC_ProtocolMemWriter_h__
#define __ARPC_ProtocolMemWriter_h__

#include "ARPC_ProtocolWriter.h"
#include <string.h>

/** 从一个指定内存区读取数据的读取器实现.
*/
class ARPC_ProtocolMemWriter : public ARPC_ProtocolWriter
{
public:
	ARPC_ProtocolMemWriter(void* b, size_t l):
	buffer_((char*)b),
	length_(l),
	wtptr_(0)
	{}

	virtual void write(const void* data, size_t len)
	{
		if(length_ < wtptr_ + len)
			return;
		::memcpy(buffer_ + wtptr_, data, len);
		wtptr_ += len;
	}

private:
	char*			buffer_;
	size_t			length_;
	size_t			wtptr_;
};


#endif//__ARPC_ProtocolMemWriter_h__