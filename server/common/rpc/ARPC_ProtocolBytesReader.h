//==============================================================================
/**
 * @file		ARPC_ProtocolBytesReader.h
 *
 * @date		2010:2:26
 *
 * @author		Lucifer<hotlala8088@gmail.com>
 *
 * Copyright (C) 2004-2010 Lucifer. All Rights Reserved.
 */
//==============================================================================
#ifndef __ARPC_ProtocolBytesReader_h__
#define __ARPC_ProtocolBytesReader_h__

#include "ARPC_ProtocolReader.h"
#include <string.h>
#include <vector>

/** 从一个bytes数组读取. */
class ARPC_ProtocolBytesReader : public ARPC_ProtocolReader
{
public:
	ARPC_ProtocolBytesReader(std::vector<ARPC_UINT8>& bytes):
	bytes_(bytes),
	rdPtr_(0)
	{}

	virtual bool read(void* data, size_t len)
	{
		if(rdPtr_ + len > bytes_.size())
			return false;
		::memcpy(data, &(bytes_[rdPtr_]), len);
		rdPtr_ += len;
		return true;
	}

private:
	std::vector<ARPC_UINT8>& bytes_;
	size_t					rdPtr_;
};


#endif//__ARPC_ProtocolBytesReader_h__