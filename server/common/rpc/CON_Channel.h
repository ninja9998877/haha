//==============================================================================
/**
 * @file		CON_Channel.h
 *
 * @date		2010:2:26
 *
 * @author		Lucifer<hotlala8088@gmail.com>
 *
 * Copyright (C) 2004-2010 Lucifer. All Rights Reserved.
 */
//==============================================================================
#ifndef __CON_Channel_h__
#define __CON_Channel_h__

#include <cstddef>

class CON_ChannelConnection;

/** 建立在真实的 CON_ChannelConnection 之上的一个虚拟通讯连接.

	对于使用者来说， CON_Channel 可以被看作与一个 CON_Connection 一样，是一个独立
	的双向网络通讯管道，可以通过 *SendingData() 向对端发送数据，并通过重载 
	handleReceived 处理对端发送过来的数据。

	在传输层，所有的这些CON_Channel 共享一个 CON_ChannelConnection 对象，进行数据
	传输。每一个CON_Channel 对象都有一个guid，用来在传输层数据中标识出属于自己的
	消息.
*/
class CON_Channel
{
public:
	friend class CON_ChannelConnection;

	/** CON_Channel 在默认构造后没有有效的 CON_ChannelConnection 与之关联，需要通
		过 CON_ChannelConnection 的两种模式与之进行关联.
	*/
	CON_Channel();

	/** CON_Channel 会在析构函数中调用close.
	*/
	virtual ~CON_Channel();

	/** 获得当前关联的 CON_ChannelConnection.
		@return NULL 表示当前没有关联.
	*/
	CON_ChannelConnection* getConn()	{ return conn_; }

	/** 监测这个 CON_Channel 当前是否有效.
		当这个 CON_Channel 与一个 CON_ChannelConnection 有效关联，返回true
	*/
	bool isValid();

	/** 主动关闭这个 CON_Channel.
	*/
	void close();

	/** 初始化需要发送的数据. */
	void initSendingData();
	/** 填充需要发送的数据. */
	void fillSendingData(void* data, size_t size);
	/** 发送被填充的数据. */
	void flushSendingData();

	/** 从管道的另一端接收到数据.
		派生类需要重载这个接口，处理接收到的数据
		@param data 数据指针.
		@param size 数据大小.
		@return 返回处理是否成功.
	*/
	virtual bool handleReceived( void* data, size_t size )	{ return true; }

protected:
	unsigned int			guid_;
	CON_ChannelConnection*	conn_;
};

#endif//__CON_Channel_h__
