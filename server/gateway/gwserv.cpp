
#include "config.h"
#include "gwserv.h"
#include "worldhandler.h"
#include "client.h"

//-------------------------------------------------------------------------
/**
 */
int 
GatewayServ::init (int argc, ACE_TCHAR *argv[])
{
	ACE_DEBUG((LM_TRACE,ACE_TEXT("Init gateway serv... ")));
	
	connect_world();

	if(ClientAcceptor::instance()->init(argc,argv) == -1 )
		return -1;
	
	
	/// \初始化记时器
	ACE_Time_Value t;
	t.set((double)1.0/(double)(TIMER_FREQ));
	this->reactor()->schedule_timer(this, NULL, t, t);
	
	
	ACE_DEBUG((LM_TRACE,ACE_TEXT("Init gateway serv succ... ")));
	return 0;
}

//-------------------------------------------------------------------------
/**
 */
int 
GatewayServ::fini (void)
{
	ACE_DEBUG((LM_TRACE,ACE_TEXT("Fini world serv... ")));

	// 退出计时器.
	this->reactor()->cancel_timer(this);

	return 0;
}

//-------------------------------------------------------------------------
/**
 */
int 
GatewayServ::handle_timeout (const ACE_Time_Value &current_time, const void *act)
{
	return 0;
}

//-------------------------------------------------------------------------
/**
 */
void GatewayServ::connect_world()
{
	ACE_INET_Addr addr(LISTEN_GATEWAY);
	ACE_Connector<WorldHandler, ACE_SOCK_CONNECTOR > connector;
	WorldHandler * p = &worldhandler_;
	if(connector.connect(p,addr) == -1)
	{
		ACE_ASSERT(0);
	}
}