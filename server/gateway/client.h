/** File generate by <hotlala8088@gmail.com> 2015/01/14  
 */

#ifndef __CLIENT_H__
#define __CLIENT_H__
#include "config.h"
#include "proto.h"
//-------------------------------------------------------------------------
/**
 * 
 */
class ClientChannel;
class ClientHandler
	:public CON_Connection
{
public:
	enum State
	{
		Idle,
		Login,
		Game,
		Logout,
		Illegal,
	};
public:
	ClientHandler();
	
public:
	int handleReceived(void* data, size_t size);
	int handle_close(ACE_HANDLE handle, ACE_Reactor_Mask close_mask);
public:
	
	ClientChannel *channel_;
};

//-------------------------------------------------------------------------
/**
 */
class ClientChannel
	:public CON_Channel
{
public:
	bool handleReceived( void* data, size_t size );
public:
	ClientHandler *handler_;
};
//-------------------------------------------------------------------------
/**
 */
class ClientAcceptor 
	: public ACE_Acceptor<ClientHandler, ACE_SOCK_ACCEPTOR>
	, public CON_ConnectionManager<ClientHandler>
{
public:
	static ClientAcceptor* instance() {static ClientAcceptor self; return &self;}
public:
	int init (int argc, ACE_TCHAR *argv[]);
	int fini (void);
	int make_svc_handler(ClientHandler *&sh);

public:
};

#endif