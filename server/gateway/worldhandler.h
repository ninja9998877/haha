/** File generate by <hotlala8088@gmail.com> 2015/01/14  
 */

#ifndef __WORLD_HANDLER_H__
#define __WORLD_HANDLER_H__

#include "config.h"
#include "proto.h"
#include "client.h"



class WorldHandler
	: public CON_ChannelConnection
{
public:
	WorldHandler():CON_ChannelConnection(true){}
public:
	CON_Channel* makeChannel() { return new ClientChannel(); }
public:
};



#endif