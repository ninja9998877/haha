
#include "client.h"
#include "gwserv.h"
//-------------------------------------------------------------------------
/**
 */
ClientHandler::ClientHandler()
{
	channel_ = new ClientChannel();
	channel_->handler_ = this;
	GatewayServ::instance()->worldhandler_.initChannelConnData(channel_);
}

int 
ClientHandler::handleReceived(void* data, size_t size)
{
	char *curData = (char*)data+sizeof(ARPC_UINT16);
	//ACE_ASSERT(len == size-sizeof(ARPC_UINT16));
	channel_->initSendingData();
	channel_->fillSendingData(curData,size-sizeof(ARPC_UINT16));
	channel_->flushSendingData();
	return size;
}

//-------------------------------------------------------------------------
/**
 */
int
ClientHandler::handle_close(ACE_HANDLE handle, ACE_Reactor_Mask close_mask)
{
	channel_->close();
	return 0;
}

//-------------------------------------------------------------------------
/**
 */
bool 
ClientChannel::handleReceived( void* data, size_t size )
{
	ACE_DEBUG((LM_TRACE,ACE_TEXT("ClientChannel::handleReceived\n")));
	handler_->fillSendBuffer(&size,sizeof(ACE_UINT16));
	handler_->fillSendBuffer(data,size);
	handler_->flushSendBuffer();
	return true; 
}

//-------------------------------------------------------------------------
/**
 */
int 
ClientAcceptor::init (int argc, ACE_TCHAR *argv[])
{
	ACE_DEBUG((LM_TRACE,ACE_TEXT("ClientAcceptor::init\n")));

	ACE_INET_Addr addr(LISTEN_CLIENT_ADDR);
	if(this->open(addr) == -1)
	{
		ACE_DEBUG((LM_INFO,ACE_TEXT("ClientAcceptor::init %s field\n"),LISTEN_CLIENT_ADDR));
		return -1;
	}

	return 0;
}

//-------------------------------------------------------------------------
/**
 */
int 
ClientAcceptor::fini (void)
{
	ACE_DEBUG((LM_TRACE,ACE_TEXT("Fini world serv... \n")));

	// �˳���ʱ��.
	this->reactor()->cancel_timer(this);

	this->close();
	return 0;
}

//-------------------------------------------------------------------------
/**
 */
int ClientAcceptor::make_svc_handler(ClientHandler *&sh)
{
	if( ACE_Acceptor<ClientHandler, ACE_SOCK_ACCEPTOR>::make_svc_handler(sh) == -1)
	{
		return -1;
	}
	connections_.push_back(sh);
	return 0;
}
