
#ifndef __GATEWAY_H__
#define __GATEWAY_H__

#include "config.h"
#include "worldhandler.h"
class GatewayServ
	:public ACE_Service_Object
{
public:
	static GatewayServ* instance() {static GatewayServ serv; return &serv;}

public:
	int init (int argc, ACE_TCHAR *argv[]);
	int fini (void);
	int handle_timeout (const ACE_Time_Value &current_time, const void *act);

public:
	void connect_world();
public:
	
	WorldHandler	worldhandler_;
};

#endif